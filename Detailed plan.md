1. Introduction
	1. Definition of an embedded system
	2. Defining the context of the thesis (what's the object of this state of the art), reminding of CIA
	3. Defining the question
2. Threats / Attacks expected
	1. Seems to be described by a lot of articles could be good for some more context
3. Working with software
	1. OverTheAir (OTA) update
		1. Concept 
			1. define
			2. Should use this : `Reliable Firmware Updates for the Information-Centric Internet of Things`
		2. Assert trust
			1. Cryptography
				1. How it works 
					1. Needs to be very brief
			2. Using the blockchain for certification
				1. How does it work
				2. Why it is needed
				3. A use-case: Paired with LoRA
			3. PUF
	2. Static analysis
		1. Integrity Assurance
			1. Explain how Prover / Verifier works
			2. Remote Attestation
			3. Time-constistency 
			4. Clock based
			5. An example : OEI attestation
		3. Abstract Interpretation
			1. defining abstract interpretation
				1. definition of the 3 basics 
					2. DSF/DSL
					3. Term Abstraction
					4. Soundness
				2. what's supposed to be done / handled
			2. (mainly) Summary of the books (Tutorial on static inference) (change the name)
				1. language and semantics
				2. abstract domains
				3. domain/program transformations
			3. In embedded ? 2 main approaches described 
				1. formalization of system and model semantics
				2. capture system semantics
		4. Proof Carrying Code
			1. Definition of PCC
				1. Defining it
					1. Intrinsic signatures
					2. Extrinsic signatures
				2. Related to
				3. Scenarios where it could be used
			2. Current challenges
4. Working with hardware
	1. How different is it from software here
		1. HW/SW Partitioning
		2. maybe an example
	2. Protected Module Architectures
		1. Objectivs
		2. Availability & Real Time Guarantees
	3. Sandboxing
		1. Definition
		2. Usable solutions
			1. IoTBox
			2. V-Sandbox
	4. SoC
		1. Remote reconfiguration
			1. Cryptographic
			2. used algorithms
			3. dynamic reconfig
		2. AXI bus stall
			1. What's the problem ?
			2. Trigger module
			3. Safety verification
		3. How sandboxing can be used against trojan
			1. ARM's TrustZone / Intel SGX
			2. Automatic sandbox generation
				1. Genral deisgn flow
				2. LTS generation
5. Conclusion

