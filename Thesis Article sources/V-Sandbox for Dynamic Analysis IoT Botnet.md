---
tags:
  - sandbox
  - embedded_device
  - constrained_devices
  - dynamic_analysis
  - security
  - machine_learning
in_plan: true
---
Sandboxing IoT devices / architectures to analyse Botnets

1. Introduction
	1. propose sandbox to deal with resource constrained IoT devicces for the following reasons :
		1. not trivial to integrate threat detection / protection soluction in these devices
		2. existing sandboxes can't bring enough data for malware analysis using machine learning
2. Related work and background
	1. Overview of IoT botnet
		1. Features :
			1. scan for appropriate targets
			2. gain access to other vulnerable devices
			3. infect ioT devices
			4. Commnicate with C&C server via IP address or URL
			5. Wait and execute commands
	2. Relevant features for IoT botnet detection
		1. Mentions IoTBOX
	3. Overview of IoT sandbox
		1. TABLE 2. Summarized dynamically features for detection of IoT Botnet.
		2. TABLE 3. Describe the IoT Sandboxes.
3. The proposed V-Sandbox
	1. Overview
		1. 8 main components :
			1. See following parts of the plan
			2. Interesting to sum up what we want
	2. ELF Metadata Extraction
	3. Sandbox config generation component
	4. Sandbox engine component
	5. Raw data preprocessing component 
	6. Sandbox recomputation component
	7. C&C simulator
	8. Share Object DB
	9. Report
4. Evaluation
	1. DataSet
		1. IoT-Pot
		2. VirusShare
		3. Internet
	2. Implement
5. Discusion
	1. IoTBOX seems to be closed source
6. Conclusion
	1. Shared on github