---
tags:
  - correctness_witness
  - program_verification
  - analysis
  - model_checking
in_plan: true
---
> The overall goal to make proofs available to engineers is probably as old as programming itself, and proofcarrying code was proposed two decades ago

> We consider witnesses as first-class exchangeable objects, stored independently from the source code and checked independently from the verifier that produced them, respecting the important principle of separation of concerns. At any time, the invariants from the correctness witness can be used to reconstruct a correctness proof to establish trust.

1. INTRODUCTION
> PCC is a mechanism where an untrusted source supplies both an executable program and a proof witness that can be checked against the program and specification by a trusted validator to establish trust in the program.
Related work : 
		- Proof Carrying Code
		- Certificates
		- Search Carrying Code (uses search scripts to guide a model checker along paths of the ARG.)
		- Proof Programs and Configurable Certification
		- Partial Verification, 3 possible outcomes :
			- find a bug
			- prove correctness
			- fail
2. CORRECTNESS WITNESSES
	1. There are only two differences between violation witnesses and correctness witnesses:
		1. Violation Witnesses restrict space whereas Correctness ones don't 
		2. Violation witnesses try to replay an error path through the program, correctness ones will try to replay the correctness of a proof
	2. Exchange Format for Correctness Witnesses
3. CONSTRUCTION OF CORRECTNESS WITNESSES
	1. CPAchecker's Verifier
	2. UltimateAutomizer’s Verifier
4. VALIDATION OF CORRECTNESS WITNESSES
	1. CPAchecker's Validator
	2. UltimateAutomizer’s Validator
5. EXPERIMENTAL EVALUATION
	1. Experiment goals
		1. Witnesses produced by a verifier can be validated by the same framework's validator (outside of internal inconsistencies)
		2. Correctness witnesses produced by A can be understood by a witness validator of B
		3. The complexity of the validation of a correctness witness is related to the contents of the witness, i.e., there are verification tasks for which a verifier can produce witnesses such that the validation uses less resources to validate the witness than the verifier used to verify the verification task
	2. Benchamrk set
	3. Experimental setup
	4. Presentation and availability
	5. results
		1. Consistency within the Same Framework.
			1. We interpret the results for our first experiment as confirmation that the witnesses produced by both tools are consistent with their own frameworks.
		2. Validation across Frameworks.
			1. Our experiment shows that for between 700 to 1 200 of 1 900 to 2 100 tasks verified by one verifier, a validator based on a different framework and different techniques not only agreed on the verdict but confirmed that no flaw was detected in the reasoning represented by the witness, whereas previously, communicating such information between different tools was entirely impossible.
		3. Effort and Feasibility of Validation Depends on Witness Contents.
			1. by different tools, the differing characteristics of the two tools may outweigh the effects of the witnesses on validation speed: Automizer is often not faster at validating the invariants contained in the witnesses, and instead is often slower than CPAchecker for those of CPAchecker’s witnesses that it can validate.
	6. Validity
		1. Benchmark selection
		2. verification tools
		3. reproducibility
6. ALTERNATIVE IMPLEM
7. CONCLUSION
	1. Software verification is a mature research area and there were many breakthroughs in the past two decades that made software verification efficient enough to be applicable on industrial scale.
	2. In verification, an engineer has to invest a significant amount of resources, but in turn gets back a wishy-washy answer true or false without any argument. The confidence in this answer is only derived from the reputation of the verification tool.
8. ARTIFACT DESC
	1. Witness Exchange format
	2. Example Witness

Note : Was made over C programs