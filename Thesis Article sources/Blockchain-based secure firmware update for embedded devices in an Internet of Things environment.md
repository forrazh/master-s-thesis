---
tags:
  - Blockchain
  - firmware_update
  - firmware_verification
  - embedded_device
in_plan: true
---
> A new firmware update scheme that utilizes a blockchain technology is proposed to securely check a firmware version, validate the correctness of firmware, and download the latest firmware for the embedded devices.

> requests its firmware update to nodes in a blockchain network and gets a response to determine whether its firmware is up-to-date or not.

Introd
Preliminaries
	1) REmote firmware updates
	2) Blockchain
Proposed secure firmware update
	1) Overview
		1) blockchain node
		2) normal node
		3) verification node
		4) vendor node
	2) Block structure
		1) Verification counter
		2) Merkle tree (tree information for calc of merkle root)
		3) Verification log
		4) model name
		5) firmware version
		6) verifier
	3) Procedure
		1) Response from a verification node v_i to a request node n_i
		2) response node n_j to request node n_i
Discussion
Conclusion
> In this paper, we presented a new firmware update scheme that securely checks a firmware version, validates the correctness of firmware, and enables downloading of the latest firmware for embedded devices in an IoT environment.