---
tags:
  - assurance_cases
  - proof_carrying_code
  - formal_semantics
in_plan: true
---

> To address this problem, we introduce assurance carrying code, a framework in which every software component in a software supply chain has its own assurance case.

Goal Structuring Notation (GSN)


Intro 
II Assurance Carrying Code
> Proof carrying code [8] is a software mechanism that allows a host system to verify properties of an application through a formal proof that accompanies the application’s executable code.

III. A PATTERN LANGUAGE FOR GSN

IV. CONCLUDING REMARKS
> We are formalizing GSN and its patterns using λ calculus.