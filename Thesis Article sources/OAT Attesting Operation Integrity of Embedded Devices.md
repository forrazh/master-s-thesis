---
tags:
  - data_integrity
  - embedded_device
  - formal_semantics
  - iot
  - security
  - remote
  - operation_execution_integrity
in_plan: true
---
>To fill this void, we first formulate a new security property for embedded devices, called “Operation Execution Integrity” or OEI.

> OAT performs the remote control-flow verification through abstract execution, which is fast and deterministic.

1. Introduction
	1. C-FLAT [2] took the first step towards control-flow integrity (CFI) attestation. But a major limitation of C-FLAT is the non-deterministic verifiability of its control-flow hashes (i.e., a given hash may not be verifiable due to the program path explosion issue). Moreover, C-FLAT does not check data integrity (i.e., data-only attacks are not covered).
	2. 2 key challenges that rise up from OEI attestation :
		1. Incomplete verification of control-flow integrity
		2.  Heavy data integrity checking
	3. Makes the following contributions :
		1. formulate a new security property
		2. hybrid attestation scheme 
			1. hashes
			2. exec traces
		3. light-weight variable integrity checking mecanism
		4. design + build OAT to realize OEI attestation on both Prover + verifier side
		5. Evaluate OAT on real-world embedded programs
2. Background
	1. Attacks on IoT devices and backends
	2. ARM TrustZone
3. Deisgn overview
	1. Example: A Vulnerable Robotic Arm
	2. Operation Execution Integrity (OEI)
		1. Remote verifier can quickly check if an exec of an operation suffered from :
			1. control flow hijack
			2. experienced critical data corruption
		2. OEI has 2 goals
			1. enabling remote detection of aforementioned attacks
			2. demonstrating the feasibility of an operation-oriented attestation that can detect to both control & critical data manipulations
		3. Operation-scoped CFI
		4. Critical variable integrity
		5. secure & optimized combination
		6. operation verifiability
	3. OAT Overview
	4. Threat model
4. Operation-scoped control flow attestation
	1. instrumented control flow events
	2. measurement scheme
	3. measurement verification
5. Critical variable integrity
	1. Critical variable id & expansion
	2. Valued based define use check
	3. pointer-based access to critical variables
6. Implementation
	1. Hw & Sw choices
7. Discussion
	1. Multithreading
	2. Interrupt handling
	3. Annotation
8. Evaluation & Analysis
	1. Micro Perf Tests
	2. Tests on Real Embedded Programs
		1. devices
			1. Syringe` Pump
			2. House Alarm System
			3. Remote Movement Controller
			4. Rover Controller 
			5. Light Controller
		2. Compile time overhead
		3. Operation Exec time & instrumentation statistics
		4. Measurement engine memory footprint & runtime overhead
		5. value based check vs addressed based check
		6. Space efficiency of Hybrid attestation
		7. veriftime
	3. Attack detection via OEI attestation
	4. Security Analysis
9. Related Work
	1. Remote Attestation
	2. Online CFI Enforcement
	3. Runtime data protection
10. Conclusion