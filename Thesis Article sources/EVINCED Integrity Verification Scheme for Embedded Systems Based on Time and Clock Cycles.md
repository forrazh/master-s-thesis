---
tags:
  - integrity_verification
  - attestation
  - embedded_device
  - introspection
in_plan: true
---
> Integrity verification is an audit mechanism that checks if the software running on a platform was not modified unintentionally.

1. INTRODUCTION
	1. Outline
2. Background
	1. Integrity Verification
		1. Mechanism that allows an ext auditor to check content recorded in the program memory
		2. dividable in two schemes
			1. based in hardware
				1. Hardware-based integrity checking uses a piece of hardware that has anti-tamper properties which is included in the platform architecture and used to obtain information about its state.
			2. based on soft
				1. software-based integrity verification, the platform’s software performs, upon request, calculations that depend on its state, the contents of program memory.
	2. Verification scheme
		1. 2 entities
			1. Verifier
				1. Under the control of a trusted authority
			2. Prover
				1. operate under uncontrolled or hostile environment
				2. target
		2. Scheme desc :
			1. Calc phase 
				1. prover receives challenge from Verifier
			2. Classification
				1. Verifier receives calc from prover
				2. V judges P's result
	3. Assumptions and Attack Nodel
		1. One micro controller / soft
		2. Rev Engineering
		3. Soft reprog
		4. Hardware modif
		5. Communication channel
			1. provides data integrity and auth
			2. established between devices
				1. assumed without hops
	4. Types of attacks considered
		1. Precomputation
			1. prevent by ensuring "freshness" of the result
		2. Memory copy
3. EVINCED INTEGRITY VERIFICATION SCHEME
	1. Calculation :  Performed by Prover
		1. INput & output
		2. Initialization
		3. Calc loop
			1. Memory division
				1. memory blocks bs bytes
			2. pseudo random path
			3. h update
			4. stop cruterion
		4. Finalization
			1. Cycle count
	2. Challenge Gen : Performed by Verifier
		1. Seed generation
			1. Seed s gen randomly
		2. Block size def
		3. Number of rounds
		4. Stop criterion
	3. Classification : Performed by Verifier
		1. Input / Output
		2. Test of the hash and cycles count
			1. both have to match to be classified ok
		3. Time analysis
4. Validation
	1. Results
	2. Discussion
5. Related work
6. Conclusion
