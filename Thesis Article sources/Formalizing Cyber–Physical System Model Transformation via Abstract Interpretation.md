---
tags:
  - abstract_interpretation
  - embedded_device
  - model_transformation
  - static_analysis
  - system_designers
  - formal_semantics
in_plan: true
---
>  Furthermore, we develop formalisms useful for encoding model properties.

I. Introd
> vast body of literature has been published on various modeling formalisms that capture system performance, dependability, safety, and security.

> The research contribution of this work is twofold. We propose:
>  1) a formalization of system and model semantics, leading to a formalization of sound model transformation, and 
>  2) a mathematical structure useful in the development of structures that capture system semantics.

II. RELATED WORK

> Model transformation research specific to CPSs primarily focuses on building hierarchical models

Ptolemy, Möbius => modeling software (hierchical modeling and model composition)
OsMoSys, SIMTHESys => model driven engineering (based on software engineering)
AToM^3 => both model transformation and model composition
CHESS => Modeling language for describing systens, based on UML 
CONCERTO => extends CHESS
Rosetta => Algebraic approach -> "However, Rosetta lacks many features required for CPS modeling, especially support for hybrid  discrete continuous formalisms"

III. ABSTRACT INTERPRETATION OF MODELS
A. Properties
B. Models
C. Correctness
D. Abstraction and Concretization
E. Model Transformation
F. Selection and Specificity

IV. TAG-OPTIONS LATTICE
A. Tag Lattice
B. Options Lattice Family
C. Options Lattice Homomorphisms
D. Tag-Options Lattice

V. CONCLUSION AND FUTURE WORK