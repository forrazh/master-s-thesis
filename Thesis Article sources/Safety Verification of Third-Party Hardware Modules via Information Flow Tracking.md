---
tags:
  - embedded_device
  - hardware_security
  - safety_verification_flow
  - FPGA
in_plan: true
---
> This paper describes how to use Information Flow Tracking (IFT) to verify the safety of bus interactions among on-chip hardware resources.

1. Introduction
2. MOtivations and background
	1. Sample SoC architecture
	2. The AXI bus stall problem
3. SAfety Verification Methodology
	1. Adressubg the AXI bus stall problem
	2. the Trigger module
	3. Simulation-Based IFT for safety verification
		1. Determine the delay limits
		2. insert the trigger modules
		3. specify the safety properties
		4. generate the IFT models
		5. create testbench
		6. verify safety properties via simulation
4. Conclusion