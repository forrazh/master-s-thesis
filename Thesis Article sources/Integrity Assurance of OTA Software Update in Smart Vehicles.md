---
tags:
  - integrity_verification
  - security
  - smart_vehicles
  - software_update
  - ota
in_plan: true
---
> we focused on the assurance of both data integrity and service integrity in smart vehicles to improve the OTA SW update service security.

1. Introduction
2. OTA SW update service analysis
	1. OTA SW update service application
	2. OTA SW update service structure
		1. third party conveys an update SW to OEM 
		2. update manager notifies vehicle / user of update
		3. vehicle request SW update and receives path 
		4. vehicle accesses update image once download is done, verify then store SW image for install. vehicle sends back success or failure
			1. when download fails go back to step 1
			2. otherwise success and change version
	3. Threats for OTA SW update service integrity
		1. OTA SW update aare rapid and economical
		2. attacker can interveneaand control the update process
		3. important to analyze threats
		4. data integrity denotews that no change or creation or deletion of data has been done by an unauthorized party
			1. modify data on the server
		5. service integrity 
			1. attacks:
				1. man-in-the-middle
				2. spoofing
				3. node capture
				4. selective forwarding
				5. sinkhole
3. Integrity assurance for OTA SW update service
	1. Security requirement for integrity
	2. method classification for inegrity
		1. data integrity
		2. service integrity
4. Conclusion
