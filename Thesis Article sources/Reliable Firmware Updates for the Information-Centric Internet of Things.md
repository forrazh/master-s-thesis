---
tags:
  - iot
  - partial_reconfiguration
  - firmware_update
  - security
  - performance_measurement
in_plan: true
---
> Hosting massive firmware roll-outs present a crucial challenge for the constrained wireless environment.

> While a chunking mechanism prepares firmware images for typically low-sized maximum transmission units (MTUs), an early Denial-of-Service (DoS) detection prevents the distribution of tampered or malformed chunks

1. Introduction 
2. The problem of firmware propagation and related work
	1. Challenges in low power regimes
	2. Firmware updates in the IoT
	3. Reliable content transfers and data management in constrained networks
3. Building blocks for reliably updating firmware with NDN
	1. Roll out campaign management
	2. firmware preparation and publication
		1. Namespace management
		2. firmware generation
		3. preparation of firmware chunks
		4. manifest description
		5. firmware upload and binary management
		6. discussion full versus incremental updates
	3. Firmware update process
		1. Firmware version discovery
		2. retrieval of firmware versions
		3. implicit consumption of firmware versions
		4. concurrent firmware updates
		5. cascading firmware updates
		6. firmware verification
		7. firmware replication on connectivity loss
		8. early denial of service detection
4. Experimental evaluation 
	1. Setup
		1. scenario and network topology
		2. sw & hw platform
		3. deployment params
	2. Firmware update progress
	3. goodput analysis
	4. link stress
	5. multiparty assessment
5. Conclusions & outlook