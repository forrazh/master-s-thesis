---
tags:
  - secure-by-design
  - platform_security
  - security_micro-architecture
  - active_defense
  - trusted_execution_environment
  - arm_trustzone
  - intel_sgx
  - hex-five_multizone
in_plan: true
---
Might be a good one for the introduction

1. Introduction
2. Evolution of Embedded Platform Security
	1. Early systems
	2. Software based security
	3. Dedicated Crypto Engine
	4. Virtualisation based security
	5. Hardware based trusted computing
3. Embedded Sec Frameworks & Bodies
	1. Global Platform
		1. Secure Elem 
		2. Trusted Exec Env
		3. Device Trust Architecture
	2. Trusted Computing Group
	3. Arm Platform Security Architecture
	4. Global Platform IoTopia
4. Vendor Specific Inolem of GP's DTA
	1. INtel Sec Guard Extension (SGX) tech
	2. Arm TrustZone tech
	3. Hex-FIVE MultiZone Sec
		1. key software components :
			1. MultiZone nanoKernel
			2. InterZone Messenger
5. Secure By Design & Sec methods
	1. What an attacker wants ? => acquire privileges access to system
	2. How it can be achieved ? => exploiting known vuln
	3. def mechanism needed based on some methods :
		1. Trust based sec
		2. Information Flow Tracking
		3. Access
	4. Future Challenges & Discussion
6. Conclusion