---
tags:
  - automotive_systems
  - generic_framework
  - ieee_802_11s
  - secup
  - security
  - software_update
  - wireless_software_update
in_plan:
---
Needed for future vehicles

SecUp enables wireless software update features such as parallel and partial software updates

Intro
> In this paper, SecUp, a generic framework enabling wireless SW updates and diagnostics is proposed.

Entire lifecycle, not only wireless

II Related Work
	a) IEEE 802.11s Mesh Networking and Its Security Features
> This system utilizes a hardware security module (HSM) for data encryption and key management as well as ensures data integrity on both the wireless interface and all ECUs of a vehicle.
System by Idrees et al
> This paper also contains a high-level description of an architecture for OTA updates, however, no implementations details or evaluation results are given.

III Framework and Architecture (of SecUp)
	a) System Architecture and Core Nodes
	b) Wireless IEEE 802.11s Network
IV. SUPPORTING THE ENTIRE VEHICLE LIFETIME
	A. Wireless SW Update Scenarios
		1) Vehicle development
		2) Vehicle assembly
		3) Vehicle Maintenance
		4) Remote SW updates
			SecUp is mainly used locally (is it really what we want ?)
	B. Wireless SW Updates in a Service Center
		1) Connecting the Vehicle to the SW Update System
		2) Gathering Information About the Vehicle
		3) Performing the Wireless SW Update
V. S ECURITY AND TRUST
> The used design approach, the DEWI1 security metric, is based on a structured system decomposition rather than a traditional approach where first attack vectors are analyzed and second corresponding countermeasures are implemented.

4 essential steps :
- Security analysis
- Extraction of the security requirements
- Security conecpt definition
- Evaluation of the defined security concept
	A. Security Threats and Attack Vectors
	B. Security Concept
	C. Application of the Security Concept to a Use Case
		1) User Authentication
		2) INterconnecting the WVI
		3) Auth between WVI and DT
		4) Parallel SW updates
		5) Data Verif
	D. Formal Security Concept Evaluation
VI. IMPLEMENTED PROTOTYPES AND DEMONSTRATORS
	A. The Developed WVI Prototype
	B. Prototypes and Implementation of the DT and Handhelds
	C. Volvo FlexECU Used for Basic Framework Evaluation
	D. Infineon AURIX as Advanced ECU Demonstrator
> To manipulate a section, a flash driver first has to erase the entire flash section before it can be filled with new content. Therefore, it is not possible to just update the value of one parameter stored in a specific section, but the entire memory section has to be rewritten. For partial SW updates this fact leads to two different approaches:
> - Delta download:
> - Partial software update#

VII. EVALUATION OF THE SECURITY CONCEPT
	A. Evaluating the Performance of the Integrated TPM
	B. Impact of Security Mechanism on the Network Layer
	C. Impact of Security Mechanisms on the SW Update Duration