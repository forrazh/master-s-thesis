---
tags:
  - sandbox
  - data_mining
  - formal_semantics
  - iot
  - software_update
  - analysis
in_plan: true
---


> While sandbox mining techniques have been proposed for Android apps, we show and discuss why they are insufficient for detecting malicious behavior in a more complex IoT system.

1. Introduction
	1. 3 contributions
		1. need for inferring security policies personalized for individial smart homes
		2. addressing this problem by proposing techniques to mine sandboxes for monitoring IoT env
		3. emprically evaluate IoT box 
			1. also compare it with sandbox mining of android apps
2. Background
	1. Smart Home platforms
	2. formal model of a smart home
	3. Mining sandboxes
		1. Researchers [9], [11] have suggested that entirely blocking/allowing access to a certain resource may be too coarsed-grained
		2. Jamrozik et al. [9] suggest mining associations between GUI elements and sensitive API access through the generation of Android GUI tests. This allows the identification of rules permitting access to sensitive resources, such as the camera, only if the user is performing specific actions on the app
		3. 2 phases
			1. exploration
				1. behaviours of an app are explored and encoded in a sandbox
			2. sandboxing
				1. behaviours that were not seen during the first phases are prohibited
				2. If the app requires a new behavior, the sandbox should prohibit it or defer the request for approval by a human user.
3. IoTBox
	1. Exploration phase
		1. Alloy analyzer
		2. some proofs / predicates
		3. static analysis
	2. Sandboxing phase
		1. done through monitoring executionss at run time
			1. dynamic analysis
		2. IoTBox uses the most recent executed events to make decisions, much like DSM, the state-of-the-art technique to mine sandboxes
4. Empirical Evaluation
	1. 2 research questions :
		1. How frequently do handcrafted security policuesn lead to false positives ?
			1. findings
		2. How effective is IoTBox ?
			1. experimental setting
				1. put flaws/unexpected things into apps that were previously analysed
				2. The execution traces from the malicious bundles of apps are input to all techniques. If a technique rejects more than 1% of actions, then we consider that it is able to detect the malicious behavior in the bundle.
			2. experimental results from staticallly produced traces
			3. experimental results on traces from test case generation
5. Discussion
	1. Risk of encoding malicious behaviours in the sandbox
	2. Limitations and tradeoffs
	3. threats to validity
6. Related work
	1. Mining sandboxes
	2. threats in an IoT system
7. Conclusion and future work