---
tags:
  - embedded_device
  - flaws
  - security
  - verification_process
  - system_perf_requirements
  - program_verification
  - partitioning
in_plan: true
---
1. Introduction
	1. For instance, if a security requirement is satisfied, but because of the security mechanisms that were introduced to fulfill this requirement, some safety requirements are not satisfied anymore, then another design iteration on the system must be performed with many different options some of them are discussed in the paper — e.g. we can modify our design, such as changing the HW/SW Partitioning for better performance, adding safety and/or security mechanisms, etc.
2. Related Work
	1. MontiSim
	2. Toolkits specialized for automotive systems such as :
		1. Medini
	3. Number of other design methodologies handle the complete design flow of ES
	4. AADL
	5. SecureUML
	6. UMLSec
3. HW/SW Partitioning with SysML-Sec
	1. SysML-Sec Method
		1. developed for the design of safe and secyre enbedded systems
	2. Partitioning in SysML-Sec
		1. In greater detail, partitioning involves determining the highlevel function and architecture of the system, and then determining which hardware components are used to execute each function or relay each communication.
		2. Once the partitioning models are developed, the system can be simulated and formally verified.
	3. Verification
		1. 2 model transformation techniques
			1. translate partitioning model into C++ code
				1. can be linked to model checking engine in order to formally check safety properties
			2. translate partitioning model into ProVerif specification
				1. soundness has been partially proved 
					1. By R. Ameur-Boulifa et al. 
					2. For SysML
	4. Design Space Exploration
		1. scoring of a design based on safety properties is under development
4. Safety, Security and Performance
	1. Security Aspects
		1. Non-Satisfied security requirements
		2. Satisfied security requirements
	2. Safety aspects
		1. Non-Satisfied safety requirements
		2. Satisfied safety requirements
	3. Performance aspects
		1. Non-Satisfied performance requirements
		2. Satisfied performance requirements
	4. Impact of mechanisms: a summary
		1. Table 1 : IMpacts of Design decisions on Safety, security and Perfs
			1. what ever we do seems to reduce performances
5. Conclusions
	1. SysML can't do everything yet
	2. the ultimate combination of safety / security / performance might not be reachable