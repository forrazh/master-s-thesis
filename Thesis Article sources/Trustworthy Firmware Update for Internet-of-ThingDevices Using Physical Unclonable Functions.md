---
tags:
  - iot
  - firmware_update
  - network_security
  - puf
  - bluetooth
link: https://ieeexplore-ieee-org.ressources-electroniques.univ-lille.fr/stamp/stamp.jsp?tp=&arnumber=8016282
authors:
  - M. A. Prada-Delgado
  - A. Vázquez-Reyes
  - I. Baturone
in_plan: true
---
> This work presents a lightweight protocol to update each device in a secure way.

1. Introduction
	1. One hot topic in this field is the use of Physical Unclonable Functions (PUFs).
	2. several works using PUFs can be found
2. Preliminaries
3. PUF-Based obfuscation
	 1. Registration
	 2. Reconstruction
4. Trustworthy communication
	1. Steps performed by the proposed protocol
		1. Secure Firmware Update Initialization: Keys and Message Structure
		2. Device Update Process: Verification, Key Derivation, and Recovery
		3. Resilience to Communication Failures: Server's Retransmission Strategy
		4. Post-Update Handling: Device Response to Redundant Update Requests
		5. Finalization and Key Transition: Server's Confirmation of Successful Firmware Update
5. Advantages of the proposal
	1. advantages that should be noticed
6. Use case in CC2541 and experimental results
7. Conclusions
	1. The use of PUFs inside the BLE hardware of IoT devices improves the security of firmware updates because the devices can be uniquely identified at low cost.