---
tags:
  - embedded_device
  - security
  - forensics
in_plan: true
---
1. Introduction
2. The need for new approaches
	1. At present, most perceived need for authentication methods tailored to CPSs relies on cryptographic techniques
	2. Highly resource-constrained devices may not be able to rely on cryptography for protection
	3. Hence, it is imperative to develop authentication mechanisms that can complement what is widely deployed today.
3. Proof-carry sensing framework
	1. Intrinsic and Extrinsic signatures
		1. Intrinsic Signatures are signatures inherent to devices, channels, and sensing environments in the physical world.
		2. Extrinsic signatures are signals and data that can be injected and monitored by a system.
	2. Vision
		1. Our key insight is to use the concept of Proof-Carrying Code [6] as inspiration for our solution, where a program (code) carries proof of its trustworthiness.
4. Representative scenarios
	1. e-Voting
	2. Transportation
	3. Shopping
5. Challenges ahead
	1. Suitable physical signatures
	2. formal model and certification
	3. safeguarding physical data
	4. computational complexity
	5. dependable software engfineering
		1. The current CPS networks are usually programmed in more than one language–a phenomenon known as polyglot programming. A common setup involves, for instance, C together with such scripting language as Lua or Python.
6. Concluding Remarks