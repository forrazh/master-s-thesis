---
tags:
  - abstract_interpretation
  - compilation
  - interpretation_techniques
  - dsl
  - formal_semantics
  - memory_management
  - program_logic
  - concurrency
  - parallelism
  - program_verification
  - model_checking
  - static_analysis
  - dynamic_analysis
  - type_theory
in_plan: true
---
> This tutorial is intended as an entry course in Abstract Interpretation, after which the reader should be ready to read the research literature on current advances in Abstract Interpretation and on the design of static analyzers for real languages.


Might need to go through the other books, is quite interesting

1 Introduction 
1.1 A First Static Analysis: Informal Presentation
Soundness:
> In the general sense, soundness states that whatever the properties inferred by an analysis, they can be trusted to hold on actual program executions. It is a very desirable property of formal methods, and one we will always ensure in this tutorial.
1.2 Scope and Applications
Pointer analysis, shape analysis
1.3 Outline 
1.4 Further Resources 

Not present in curr:

2 Elements of Abstract Interpretation
2.1 Order Theory
2.2 Fixpoints 
2.3 Approximations
2.4 Summary
2.5 Bibliographic Notes

3 Language and Semantics
3.1 Syntax
3.2 Atomic Statement Semantics
3.3 Denotational-Style Semantics
3.4 Equation-Based Semantics 
3.5 Abstract Semantics
3.6 Bibliographic Notes

4 Non-Relational Abstract Domains 
4.1 Value and State Abstractions 
4.2 The Sign Domain
4.3 The Constant Domain 
4.4 The Constant Set Domain
4.5 The Interval Domain 
4.6 Advanced Abstract Tests 
4.7 Advanced Iteration Techniques
4.8 The Congruence Domain
4.9 The Cartesian Abstraction 
4.10 Summary
4.11 Bibliographic Notes

5 Relational Abstract Domains
5.1 Motivation
5.2 The Affine Equalities Domain (Karr’s Domain)
5.3 The Affine Inequalities Domain (Polyhedra Domain)
5.4 The Zone and Octagon Domains
5.5 The Template Domain .
5.6 Summary
5.7 Bibliographic Notes 

6 Domain Transformers 
6.1 The Lattice of Abstractions
6.2 Product Domains 
6.3 Disjunctive Completions
6.4 Summary 
6.5 Bibliographic Notes

7 Conclusion 
7.1 Summary
7.2 Principles 
7.3 Towards the Analysis of Realistic Programs
