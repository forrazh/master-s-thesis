---
tags:
  - FPGA
  - partial_reconfiguration
  - remote
  - secure
  - dpr
authors:
  - JO VLIEGEN
  - NELE MENTENS
  - INGRID VERBAUWHEDE
link: https://dl-acm-org.ressources-electroniques.univ-lille.fr/doi/pdf/10.1145/2629423
in_plan: true
---
1. Introduction
2. Related Work
	1. description of a system for remote self reconfiguring
		1. data
			1. confidentiality
				1. through DES algorithm
			2. authentification
				1. with MD5 digestion
		2. ...
3. Securing the remote communication
	1. Methodology
	2. Thread model
		1. channel
		2. device
	3. cryptogrphic protocol
		1. long term or session key
			1. motivated to :
				1. limit amount of available cipher texts
				2. limit exposure 
				3. avoid long-term storage of secret keys
				4. create independence across comm sessions or appls
		2. Key establishment by key agreenebt ir key transfer protocol
		3. key agreement through symmetric or public key cryptography
		4. Public key cryptography with or without trusted third party
	4. algorithms
		1. STS uses a specific set of operations
			1. rng
			2. elliptic curve point operations
			3. symmetric key encrypion / decryption
			4. gen / verif of
				1. digital signatures
				2. MAC
4. Implementation of the cryptographic core
	1. Elliptic curve processor (ECP)
		1. Datapath
		2. control path
		3. ECP programs
	2. Other components
	3. Resulting cryptocore
5. Dynamic partial reconfiguration approach
	1. Top level System
	2. Static partition
		1. Network transport layer
		2. sequence of operations
	3. Reconfigurable partition
6. results of the prototype
	1. Implem
		1. Area
		2. Speed
	2. Security
		1. 2 main issues
			1. bitstream contains the private key
			2. storing the private key in the passthrough memory is problematic because of the fact that this key resides at a constant address in a well-defined set of BRAMs.
7. Conclusions and future work