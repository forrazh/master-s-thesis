---
tags:
  - Blockchain
  - firmware
  - security
  - iot
  - hyperledger
authors:
  - Minsung Son
  - Heeyoul Kim
in_plan: true
---
> we propose a new firmware management architecture using blockchains and IPFS (The InterPlanetary File System).

> However, IoT device version management and IPFS URL integrity cannot be guaranteed.

Introduction
> in 2018, the global smart factory demand market is expected to grow at an average annual rate of 8% until 2020, and the device and ICT markets that constitute the smart factory supply market are expected to grow at average annual rates of 7.8% and 8.1%, respectively.

II. BACKGROUND
	A) Blockchain
	B) Hyperledger Fabric
> The Hyperledger Fabric [7] is one of the Linux Foundation's hyperledger projects and is an open source based private blockchain platform being developed under the leadership of IBM.

	C. IPFS ( The InterPlanetary File System )
> It is based on a peer-to-peer system and is similar to the Bit Torrent.

III. PROPOSED SYSTEM
	A. Network Setting
		1) Internal network
		2) External network
		3) Broker (peer belonging to both external and internal networks)
	B. Device registration
	C. Firmware distribution
	D. Firmware check & update
	E. Implementation
IV. CONCLUSION
> safe firmware update is possible because firmware falsification through firmware roll-back or man-in-the-middle attacks, which is a problem in existing firmware systems, can be prevented