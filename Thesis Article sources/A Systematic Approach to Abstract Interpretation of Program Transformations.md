---
tags:
  - abstract_interpretation
  - analysis
  - programming_languages
in_plan: true
---
Concept is well understood but we haven't given much attention to analysis of program transformations.
A complete one should handle many other aspects such as :
- backtracking
- generic traversals
- analysis specific concerns :
	- interprocedurality
	- fixpoints

Currently works on the Stratego Transformation Language :
- singleton analysis for constant propagation
- sort analysis for type checking
- locally-illsorted sort analysis that can additionnaly valte type changing generic traversals

I/ Intro
Sound static analyses
useful in enabling compiler optimizations (e.g., constant propagation [3], purity analysis [21]) (and other things).
Unfortunately, the analysis of program transformations has not seen much attention so far.

DSF : 
Some languages :
- Stratego
- Rascal
- Maude
aim to simplify dev of program transformations but we dont have that in general purpose lnaguages and litt on static anlysis provides only little guidance.

Term Abstraction: 
Programs:
=> first class in program transformation
=> represented as terms (e.g.: ast)
anaylisys needs good abstraction for terms (syntactic sorts, grammars)
"The design of good abstract domains is inherent to the development of any abstract interpreter and cannot be avoided."

Soundness :
Difficult task
transformation languages often doewn't have formal semntics => hard to verify the abstract interpreter

Focuses on Transformation languages

It is based on the well-founded theory of compositional soundness proofs [13] and reusable analysis components [12].

Term abstraction delegated to an interface (so just implement it)

2 Illustrating Example: Singleton Analysis
Explain the basis needed
2.1 Abstract Interpreter for Program Transformations = Generic Interpreter + Term Abstraction
> Soundly approximating these features in an analysis is not easy, and resolving this challenge for each analysis anew is impractical.

2.2 A Singleton Term Abstraction
2.3 Soundness
2.4 Summary

2 steps :
- develop a generic interpreter
- define a term abstraction and instantiate the generic interpreter

3 Generic Interpreters for Program Transformations
3.1 The Program Transformation Language Stratego
3.2 A Generic Interpreter for Stratego
3.3 The Term Abstraction

4 Sort Analysis
- inter procedural sort analysis for stratego
4.1 Sorts and Sort Contexts
4.2 Abstract Term Operations
4.3 Sort Analysis and Generic Traversals

5 Locally Ill-Sorted Sort Analysis
> Many program transformations, like a compiler, translate terms from one sort to terms of another sort

5.1 Term Abstraction for Ill-Sorted Terms
- The key idea is to use a term abstraction which can represent terms with wellsorted leafs and an possible ill-sorted prefix
5.2 Abstract Term Operations
5.3 Analyzing Type-Changing Generic Traversals

Doesn't talk about complexity