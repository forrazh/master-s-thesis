---
tags:
  - secure
  - embedded_device
  - protected_module_architectures
---
1. Introduction
2. Background: Protected module architectures
	1. Self protecting modules
	2. Sancus and embedded PMAs
3. Objectives
	1. central features :
		1. Zero-Software TCB
		2. Real-Time Compliance
		3. Secure multitasking
4. Working towards Availability and Real-Time Guarantees
	1. Interruptible Isolated Execution
		1. Interrupt-Serving SPMs
		2. Interruptible SPMs
		3. Atomicity constraints
	2. REal-Time secure multitasking
		1. multithreading model
		2. protected scheduler
		3. asynchronous communication
5. Related work
	1. Embedded security architectures
	2. High end security architectures
6. Conclusion