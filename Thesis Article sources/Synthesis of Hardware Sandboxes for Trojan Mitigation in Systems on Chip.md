---
tags:
  - FPGA
  - sandbox
  - hardware_security
in_plan: true
---
> We have designed a tool that automatically generates the sandbox and facilitates their integration into system-onchip.

> All our results showed 100% Trojan detection and mitigation, with only a minimal increase in resource overhead and no performance decrease.

1. Introduction
	1. Hardware Sandboxing was introduced in [1] as a means to overcome the shortcomings of traditional static Trojan mitigation methods, which use intense simulation, verification, and physical tests to detect the evidence of malicious components before system deployment.
2. Related  work
	1. current approaches are statuc 
	2. used at ll levels of IC design process
	3. Recently a synthesis approach along with a tool called Lily has been proposed for LTL synthesis to Verilog.
3. Hardware sandboxing concepts and structure
	1. While ARM’s TrustZone isolates sensitive parts of the system into a trusted zone and gives unlimited access to the rest of the system to non-trusted IPs, hardware sandboxing does the inverse by giving unlimited access to all system resources to trusted components and encapsulates nontrusted components within Hardware Sandboxes
4. Proposed design flow
	1. background and definitions
	2. automatic sandbox generation
		1. general design flow
		2. LTS generation
5. Case studies and evaluation
6. Conclusion
	1. need to investigate the use of hardware sandboxes as flexible isolation infrastructe like SELinux would