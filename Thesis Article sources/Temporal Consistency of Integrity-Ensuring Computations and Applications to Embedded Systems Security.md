---
tags:
  - embedded_device
  - remote
  - attestation
  - temporal_consistency
in_plan: true
---
> In this paper, we systematically explore the notion of temporal consistency of cryptographic integrity-ensuring functions.

1. Introduction
	1. Verifier + prover comes once again
2. Temporal Consistency
	1. Remote attestation
		1. 3 approaches :
			1. Hardware
				1. typically rely on security provided by a Trusted Platform Module
			2. software
				1. very low cost
			3. hybrid
				1. have been proposed to overcome limitations of purely software-based techniques.
	2. RA Blueprint
		1. Description of Remote Attestation (challenge, computation, etc)
	3. Trivial approach
	4. Attestation target
3. Modeling temporal consistency
4. Temporal consistency mechanisms
	1. simple approaches
		1. no-lock
		2. all-lock
		3. all-lock-ext
	2. sliding locks
		1. decreasing lock
		2. increasing lock
		3. extended increasing lock
	3. MIxing copyting with locking
		1. copy lock
		2. copy lock & writeback
	4. Variations on the theme
		1. Non sequential functions
		2. adaptive locking
		3. laxy copy
	5. Uninterruptibility vs Locking
		1. consistency achieved by temporarily locking parts of memory
	6. Memory access violations
	7. inconsistency detection
5. Implementation & Evaluation
	1. Hydra
		1. Hybrid RA for devices with a MMU
	2. Experimental setup
	3. Experimental results: Primitives
	4. Experimental results: Mechanisms
	5. Implementation of Inconsistency detection
	6. Experimental results: Inconsistency detection
6. Related work
7. Conclusions