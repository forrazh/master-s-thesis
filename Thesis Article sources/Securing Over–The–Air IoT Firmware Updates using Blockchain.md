---
tags:
  - Blockchain
  - firmware_update
  - iot
  - network_security
authors:
  - Sarra Alqahtani
  - Xinchi He
  - Rose Gamble
  - Mauricio Papa
link: https://dl-acm-org.ressources-electroniques.univ-lille.fr/doi/pdf/10.1145/3312614.3312649
in_plan: true
---
The system was evaluated for scalability and response to denial of service (DoS) and man–in–the–middle (MitM) attacks.

1. Introduction
	1. Relying on a blockchain framework instead of a centralized approach has the following advantages
		1. easy to keep track of all events associated with the firmware update
		2. manufacturers can use smart contracts to specify update conditions in a flexible manner
		3. distributed nature of the blockchain frameworks makes them more resilient to network failures and cyber attacks
	2. Try to replace the single point of failure of the OTA update with a blockchain
2. Blockchain basics and justification
3. Related work
4. approach
	1. Architecture
	2. Firmware update verification
	3. threat model and incident handling
5. Implementation
	1. IoT chip (esp8266)
	2. Vendor service
	3. blockchain
		1. Hyperledger fabic 1.1.0
6. Experimental results and evaluation
	1. Denial-of-Service attack
	2. MitM attack
	3. Performance and scalability
7. Conclusions and future work
	1. proof-of-concept system was built to demonstrate the feasibility of the approach using the Hyperledger Fabric (blockchain), Chaincode (smart contracts) and the Wemos D1 Mini board (ESP8266based IoT device).
	2. future work will concentrate on 
		1. incorparating a diverse variety of IoT devices
		2. adding a layer of intelligence that facilitates the creation of smart contracts
		3. improving monitoring capabilities of the entire system