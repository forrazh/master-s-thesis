---
tags:
  - Blockchain
  - iot
  - lora
  - firmware_update
authors:
  - A. Anastasiou
  - P. Christodoulou
  - K. Christodoulou
  - V. Vassiliou
  - Z. Zinonos
link: https://ieeexplore-ieee-org.ressources-electroniques.univ-lille.fr/stamp/stamp.jsp?tp=&arnumber=9183571
in_plan: true
---
> Recently, LoRa Alliance has released a new specification (FUOTA) on how to perform firmware updates using LoRa technology.

1. Introduction
	1. In the Mirai attack, criminals found a way to perform the largest DDoS attack ever known. Authors in [2] have concluded that the main reason for the attack being so successful, was the factory credentials that were left unchanged along with the inadequate firmware updates.
	2. This paper uses 2 state of the art procedures to support firmware updates on IoT devices :
		1. Blockchain smart contracts
		2. FUOTA (made by the LoRa Alliance)
2. Background information
	1. Blockchain
	2. LoRa
		1. LoRaWAN has 3 classes
			1. A 
				1. bidirectional communications
				2. 2 short downlink receive windows
				3. transmission slot based on own communication needs 
			2. B
				1. Higher number of received slots
				2. open extra receive windows when compared with Class-A
			3. C
				1. Alomost continuoussly open receiving window mechanisms
				2. closed only when receiving
				3. consumes more power than the 2 other classes
3. Firmware update using Blockchain 
	1. Consists of 11 steps
4. Evaluation
	1. Number of Nodes Evaluation
	2. Firmware Size Evaluation
	3. Blockchain Evaluation
5. Conclusion