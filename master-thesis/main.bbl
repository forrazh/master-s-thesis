\begin{thebibliography}{10}

\bibitem{aloseel_analytical_2021}
A.~Aloseel, H.~He, C.~Shaw, and M.~A. Khan, ``Analytical {Review} of {Cybersecurity} for {Embedded} {Systems},'' {\em IEEE Access}, vol.~9, pp.~961--982, 2021.
\newblock Conference Name: IEEE Access.

\bibitem{leloglu_review_2016}
E.~Leloglu, ``A {Review} of {Security} {Concerns} in {Internet} of {Things},'' {\em Journal of Computer and Communications}, vol.~5, pp.~121--136, Dec. 2016.
\newblock Number: 1 Publisher: Scientific Research Publishing.

\bibitem{lee_blockchain-based_2017}
B.~Lee and J.-H. Lee, ``Blockchain-based secure firmware update for embedded devices in an {Internet} of {Things} environment,'' {\em The Journal of Supercomputing}, vol.~73, pp.~1152--1167, Mar. 2017.

\bibitem{son_blockchain-based_2019}
M.~Son and H.~Kim, ``Blockchain-based secure firmware management system in {IoT} environment,'' in {\em 2019 21st {International} {Conference} on {Advanced} {Communication} {Technology} ({ICACT})}, pp.~142--146, Feb. 2019.
\newblock ISSN: 1738-9445.

\bibitem{castro_evinced_2017}
C.~G.~d. Castro, S.~d.~M. Câmara, L.~F. R. d.~C. Carmo, and D.~R. Boccardo, ``{EVINCED}: {Integrity} {Verification} {Scheme} for {Embedded} {Systems} {Based} on {Time} and {Clock} {Cycles},'' in {\em 2017 {IEEE} 15th {Intl} {Conf} on {Dependable}, {Autonomic} and {Secure} {Computing}, 15th {Intl} {Conf} on {Pervasive} {Intelligence} and {Computing}, 3rd {Intl} {Conf} on {Big} {Data} {Intelligence} and {Computing} and {Cyber} {Science} and {Technology} {Congress}({DASC}/{PiCom}/{DataCom}/{CyberSciTech})}, pp.~788--795, Nov. 2017.

\bibitem{suomela_answer_2010}
J.~Suomela, ``Answer to "{What}'s the difference between an invariant and an "inductive" invariant?",'' Nov. 2010.

\bibitem{wikipedia_power_2024}
Wikipedia, ``Power set,'' Mar. 2024.
\newblock Page Version ID: 1214547690.

\bibitem{jarus_formalizing_2019}
N.~Jarus, S.~S. Sarvestani, and A.~Hurson, ``Formalizing {Cyber}–{Physical} {System} {Model} {Transformation} {Via} {Abstract} {Interpretation},'' in {\em 2019 {IEEE} 19th {International} {Symposium} on {High} {Assurance} {Systems} {Engineering} ({HASE})}, (Hangzhou, China), pp.~107--114, IEEE, Jan. 2019.

\bibitem{apple_support_secure_nodate}
A.~Support, ``Secure {Enclave}.''

\bibitem{kietzmann_performance_2021}
P.~Kietzmann, L.~Boeckmann, L.~Lanzieri, T.~C. Schmidt, and M.~Wählisch, ``A {Performance} {Study} of {Crypto}-{Hardware} in the {Low}-end {IoT},'' 2021.
\newblock Publication info: Published elsewhere. EWSN'21: Proceedings of the 2021 International Conference on Embedded Wireless Systems and Networks.

\bibitem{kocabas_poster_2011}
U.~Kocabas, A.~R. Sadeghi, C.~Wachsmann, and S.~Schulz, ``Poster: practical embedded remote attestation using physically unclonable functions,'' in {\em Proceedings of the 18th {ACM} conference on {Computer} and communications security}, {CCS} '11, (New York, NY, USA), pp.~797--800, Association for Computing Machinery, Oct. 2011.

\bibitem{siddiqui_evolution_2020}
F.~Siddiqui and S.~Sezer, ``Evolution of {Embedded} {Platform} {Security} {Technologies}: {Past}, {Present} \& {Future} {Challenges},'' in {\em 2020 {IEEE} 33rd {International} {System}-on-{Chip} {Conference} ({SOCC})}, pp.~13--18, Sept. 2020.
\newblock ISSN: 2164-1706.

\bibitem{mead_defeating_2016}
J.~Mead, C.~Bobda, and T.~J. Whitaker, ``Defeating drone jamming with hardware sandboxing,'' in {\em 2016 {IEEE} {Asian} {Hardware}-{Oriented} {Security} and {Trust} ({AsianHOST})}, pp.~1--6, Dec. 2016.

\bibitem{gollmann_computer_2010}
D.~Gollmann, ``Computer security,'' {\em WIREs Computational Statistics}, vol.~2, no.~5, pp.~544--554, 2010.
\newblock \_eprint: https://wires.onlinelibrary.wiley.com/doi/pdf/10.1002/wics.106.

\bibitem{roy_lightweight_2022}
S.~Roy, J.~Li, B.-J. Choi, and Y.~Bai, ``A lightweight supervised intrusion detection mechanism for {IoT} networks,'' {\em Future Generation Computer Systems}, vol.~127, pp.~276--285, Feb. 2022.

\bibitem{carpent_temporal_2018}
X.~Carpent, K.~Eldefrawy, N.~Rattanavipanon, and G.~Tsudik, ``Temporal {Consistency} of {Integrity}-{Ensuring} {Computations} and {Applications} to {Embedded} {Systems} {Security},'' in {\em Proceedings of the 2018 on {Asia} {Conference} on {Computer} and {Communications} {Security}}, {ASIACCS} '18, (New York, NY, USA), pp.~313--327, Association for Computing Machinery, May 2018.

\bibitem{steger_efficient_2018}
M.~Steger, C.~A. Boano, T.~Niedermayr, M.~Karner, J.~Hillebrand, K.~Roemer, and W.~Rom, ``An {Efficient} and {Secure} {Automotive} {Wireless} {Software} {Update} {Framework},'' {\em IEEE Transactions on Industrial Informatics}, vol.~14, pp.~2181--2193, May 2018.
\newblock Conference Name: IEEE Transactions on Industrial Informatics.

\bibitem{kim_integrity_2019}
G.~Kim and I.~Y. Jung, ``Integrity {Assurance} of {OTA} {Software} {Update} in {Smart} {Vehicles},'' {\em International Journal on Smart Sensing and Intelligent Systems}, vol.~12, pp.~1--8, Jan. 2019.

\bibitem{he_securing_2019}
X.~He, S.~Alqahtani, R.~Gamble, and M.~Papa, ``Securing {Over}-{The}-{Air} {IoT} {Firmware} {Updates} using {Blockchain},'' in {\em Proceedings of the {International} {Conference} on {Omni}-{Layer} {Intelligent} {Systems}}, {COINS} '19, (New York, NY, USA), pp.~164--171, Association for Computing Machinery, May 2019.

\bibitem{andrade_managing_2017}
C.~E. Andrade, S.~D. Byers, V.~Gopalakrishnan, E.~Halepovic, M.~Majmundar, D.~J. Poole, L.~K. Tran, and C.~T. Volinsky, ``Managing {Massive} {Firmware}-{Over}-{The}-{Air} {Updates} for {Connected} {Cars} in {Cellular} {Networks},'' in {\em Proceedings of the 2nd {ACM} {International} {Workshop} on {Smart}, {Autonomous}, and {Connected} {Vehicular} {Systems} and {Services}}, {CarSys} '17, (New York, NY, USA), pp.~65--72, Association for Computing Machinery, Oct. 2017.

\bibitem{gundogan_reliable_2021}
C.~Gündoğan, C.~Amsüss, T.~C. Schmidt, and M.~Wählisch, ``Reliable firmware updates for the information-centric internet of things,'' in {\em Proceedings of the 8th {ACM} {Conference} on {Information}-{Centric} {Networking}}, {ICN} '21, (New York, NY, USA), pp.~59--70, Association for Computing Machinery, Sept. 2021.

\bibitem{noauthor_semver_nodate}
``{SemVer} {Compatibility} - {The} {Cargo} {Book}.''

\bibitem{kaspersky_what_2023}
Kaspersky, ``What is {Cryptography}?,'' Sept. 2023.
\newblock Section: Resource Center.

\bibitem{anssi_mecanismes_nodate}
ANSSI, ``Mécanismes cryptographiques {\textbar} {ANSSI}.''

\bibitem{zandberg_secure_2019}
K.~Zandberg, K.~Schleiser, F.~Acosta, H.~Tschofenig, and E.~Baccelli, ``Secure {Firmware} {Updates} for {Constrained} {IoT} {Devices} {Using} {Open} {Standards}: {A} {Reality} {Check},'' {\em IEEE Access}, vol.~7, pp.~71907--71920, 2019.
\newblock Conference Name: IEEE Access.

\bibitem{mandt_demystifying_nodate}
T.~Mandt, M.~Solnik, and D.~Wang, ``Demystifying the {Secure} {Enclave} {Processor},''

\bibitem{ledger_what_nodate}
Ledger, ``What {Is} {Blockchain}?.''

\bibitem{binance_what_nodate}
Binance, ``What {Is} a {Blockchain} {Consensus} {Algorithm}?.''

\bibitem{wikipedia_smart_2023}
Wikipedia, ``Smart contract,'' Dec. 2023.
\newblock Page Version ID: 1188555829.

\bibitem{augustin_study_2016}
A.~Augustin, J.~Yi, T.~Clausen, and W.~M. Townsley, ``A {Study} of {LoRa}: {Long} {Range} \& {Low} {Power} {Networks} for the {Internet} of {Things},'' {\em Sensors}, vol.~16, p.~1466, Sept. 2016.
\newblock Number: 9 Publisher: Multidisciplinary Digital Publishing Institute.

\bibitem{anastasiou_iot_2020}
A.~Anastasiou, P.~Christodoulou, K.~Christodoulou, V.~Vassiliou, and Z.~Zinonos, ``{IoT} {Device} {Firmware} {Update} over {LoRa}: {The} {Blockchain} {Solution},'' in {\em 2020 16th {International} {Conference} on {Distributed} {Computing} in {Sensor} {Systems} ({DCOSS})}, pp.~404--411, May 2020.
\newblock ISSN: 2325-2944.

\bibitem{sun_oat_2020}
Z.~Sun, B.~Feng, L.~Lu, and S.~Jha, ``{OAT}: {Attesting} {Operation} {Integrity} of {Embedded} {Devices},'' in {\em 2020 {IEEE} {Symposium} on {Security} and {Privacy} ({SP})}, pp.~1433--1449, May 2020.
\newblock ISSN: 2375-1207.

\bibitem{beyer_correctness_2016}
D.~Beyer, M.~Dangl, D.~Dietsch, and M.~Heizmann, ``Correctness witnesses: exchanging verification results between verifiers,'' in {\em Proceedings of the 2016 24th {ACM} {SIGSOFT} {International} {Symposium} on {Foundations} of {Software} {Engineering}}, {FSE} 2016, (New York, NY, USA), pp.~326--337, Association for Computing Machinery, Nov. 2016.

\bibitem{eldefrawy_smart_2012}
K.~Eldefrawy, G.~Tsudik, A.~Francillon, and D.~Perito, ``Smart: secure and minimal architecture for (establishing dynamic) root of trust.,'' in {\em Ndss}, vol.~12, pp.~1--15, 2012.

\bibitem{wikipedia_control_2024}
Wikipedia, ``Control flow,'' Feb. 2024.
\newblock Page Version ID: 1209097766.

\bibitem{wikipedia_soundness_2023}
Wikipedia, ``Soundness,'' Dec. 2023.
\newblock Page Version ID: 1191952391.

\bibitem{mine_tutorial_2017}
A.~Miné, ``Tutorial on {Static} {Inference} of {Numeric} {Invariants} by {Abstract} {Interpretation},'' {\em Foundations and Trends® in Programming Languages}, vol.~4, pp.~120--372, Dec. 2017.
\newblock Publisher: Now Publishers, Inc.

\bibitem{keidel_systematic_2020}
S.~Keidel and S.~Erdweg, ``A {Systematic} {Approach} to {Abstract} {Interpretation} of {Program} {Transformations},'' in {\em Verification, {Model} {Checking}, and {Abstract} {Interpretation}} (D.~Beyer and D.~Zufferey, eds.), Lecture {Notes} in {Computer} {Science}, (Cham), pp.~136--157, Springer International Publishing, 2020.

\bibitem{trippel_fuzzing_2022}
T.~Trippel, K.~G. Shin, A.~Chernyakhovsky, G.~Kelly, D.~Rizzo, and M.~Hicks, ``Fuzzing {Hardware} {Like} {Software},'' pp.~3237--3254, 2022.

\bibitem{vittorini_osmosys_2004}
V.~Vittorini, M.~Iacono, N.~Mazzocca, and G.~Franceschinis, ``The {OsMoSys} approach to multi-formalism modeling of systems,'' {\em Software \& Systems Modeling}, vol.~3, pp.~68--81, Mar. 2004.

\bibitem{barbierato_simthesyser_nodate}
E.~Barbierato, M.~Gribaudo, and M.~Iacono, ``{SIMTHESysER}: a tool generator for the performance evaluation of multiformalism models,''

\bibitem{kballou_answer_2021}
kballou, ``Answer to "{What} does square subset and square union symbol mean?",'' May 2021.

\bibitem{wu_proof-carrying_2017}
M.~Wu, F.~M.~Q. Pereira, J.~Liu, H.~S. Ramos, M.~S. Alvim, and L.~B. Oliveira, ``Proof-{Carrying} {Sensing}: {Towards} {Real}-{World} {Authentication} in {Cyber}-{Physical} {Systems},'' in {\em Proceedings of the 15th {ACM} {Conference} on {Embedded} {Network} {Sensor} {Systems}}, {SenSys} '17, (New York, NY, USA), pp.~1--6, Association for Computing Machinery, Nov. 2017.

\bibitem{wikipedia_proof-carrying_nodate}
Wikipedia, ``Proof-carrying code - {Wikipedia}.''

\bibitem{necula_proof-carrying_1997}
G.~C. Necula, ``Proof-carrying code,'' in {\em Proceedings of the 24th {ACM} {SIGPLAN}-{SIGACT} symposium on {Principles} of programming languages}, {POPL} '97, (New York, NY, USA), pp.~106--119, Association for Computing Machinery, Jan. 1997.

\bibitem{matsuno_assurance_2021}
Y.~Matsuno, Y.~Yamagata, H.~Nishihara, and Y.~Hosokawa, ``Assurance {Carrying} {Code} for {Software} {Supply} {Chain},'' in {\em 2021 {IEEE} {International} {Symposium} on {Software} {Reliability} {Engineering} {Workshops} ({ISSREW})}, pp.~276--277, Oct. 2021.

\bibitem{beyer_witness_2015}
D.~Beyer, M.~Dangl, D.~Dietsch, M.~Heizmann, and A.~Stahlbauer, ``Witness validation and stepwise testification across software verifiers,'' in {\em Proceedings of the 2015 10th {Joint} {Meeting} on {Foundations} of {Software} {Engineering}}, {ESEC}/{FSE} 2015, (New York, NY, USA), pp.~721--733, Association for Computing Machinery, Aug. 2015.

\bibitem{satchidanandan_dynamic_2017}
B.~Satchidanandan and P.~R. Kumar, ``Dynamic {Watermarking}: {Active} {Defense} of {Networked} {Cyber}–{Physical} {Systems},'' {\em Proceedings of the IEEE}, vol.~105, pp.~219--240, Feb. 2017.
\newblock Conference Name: Proceedings of the IEEE.

\bibitem{noauthor_hardware_nodate}
``Hardware vs {Software} - {Difference} and {Comparison} {\textbar} {Diffen}.''

\bibitem{salewski_diverse_2005}
F.~Salewski, D.~Wilking, and S.~Kowalewski, ``Diverse hardware platforms in embedded systems lab courses: a way to teach the differences,'' {\em ACM SIGBED Review}, vol.~2, pp.~70--74, Oct. 2005.

\bibitem{apvrille_harmonizing_2019}
L.~Apvrille and L.~W. Li, ``Harmonizing {Safety}, {Security} and {Performance} {Requirements} in {Embedded} {Systems},'' in {\em 2019 {Design}, {Automation} \& {Test} in {Europe} {Conference} \& {Exhibition} ({DATE})}, pp.~1631--1636, Mar. 2019.
\newblock ISSN: 1558-1101.

\bibitem{hategekimana_secure_2018}
F.~Hategekimana, J.~Mandebi~Mbongue, M.~J.~H. Pantho, and C.~Bobda, ``Secure {Hardware} {Kernels} {Execution} in {CPU}+{FPGA} {Heterogeneous} {Cloud},'' in {\em 2018 {International} {Conference} on {Field}-{Programmable} {Technology} ({FPT})}, pp.~182--189, Dec. 2018.

\bibitem{van_bulck_towards_2016}
J.~Van~Bulck, J.~Noorman, J.~T. Mühlberg, and F.~Piessens, ``Towards availability and real-time guarantees for protected module architectures,'' in {\em Companion {Proceedings} of the 15th {International} {Conference} on {Modularity}}, (Málaga Spain), pp.~146--151, ACM, Mar. 2016.

\bibitem{apvrille_sysml-sec_2015}
L.~Apvrille and Y.~Roudier, ``{SysML}-{Sec}: {A} {Model} {Driven} {Approach} for {Designing} {Safe} and {Secure} {Systems},'' in {\em 3rd {International} {Conference} on {Model}-{Driven} {Engineering} and {Software} {Development}, {Special} session on {Security} and {Privacy} in {Model} {Based} {Engineering}}, (Angers, France), SCITEPRESS Digital Library, Feb. 2015.

\bibitem{apvrille_ttool_2008}
L.~Apvrille, ``{TTool} for {DIPLODOCUS}: an environment for design space exploration,'' in {\em Proceedings of the 8th international conference on {New} technologies in distributed systems}, {NOTERE} '08, (New York, NY, USA), pp.~1--4, Association for Computing Machinery, June 2008.

\bibitem{wikipedia_system_2024}
Wikipedia, ``System on a chip,'' Mar. 2024.
\newblock Page Version ID: 1213565384.

\bibitem{noauthor_fpga_nodate}
``{FPGA} {Prototyping} to {Structured} {ASIC} {Production} to {Reduce} {Cost}, {Risk} \& {TTM}.''

\bibitem{apple_support_apple_nodate}
A.~Support, ``Apple {SoC} security.''

\bibitem{noauthor_what_nodate}
``What is a side-channel attack?.''

\bibitem{vliegen_secure_2014}
J.~Vliegen, N.~Mentens, and I.~Verbauwhede, ``Secure, {Remote}, {Dynamic} {Reconfiguration} of {FPGAs},'' {\em ACM Transactions on Reconfigurable Technology and Systems}, vol.~7, pp.~35:1--35:19, Dec. 2014.

\bibitem{meza_safety_nodate}
A.~Meza, F.~Restuccia, R.~Kastner, and J.~Oberg, ``Safety {Verification} of {Third}-{Party} {Hardware} {Modules} via {Information} {Flow} {Tracking},''

\bibitem{hu_hardware_2021}
W.~Hu, A.~Ardeshiricham, and R.~Kastner, ``Hardware {Information} {Flow} {Tracking},'' {\em ACM Computing Surveys}, vol.~54, pp.~83:1--83:39, May 2021.

\bibitem{prada-delgado_trustworthy_2017}
M.~A. Prada-Delgado, A.~Vázquez-Reyes, and I.~Baturone, ``Trustworthy firmware update for {Internet}-of-{Thing} {Devices} using physical unclonable functions,'' in {\em 2017 {Global} {Internet} of {Things} {Summit} ({GIoTS})}, pp.~1--5, June 2017.

\bibitem{schulz_short_2011}
S.~Schulz, A.-R. Sadeghi, and C.~Wachsmann, ``Short paper: lightweight remote attestation using physical functions,'' in {\em Proceedings of the fourth {ACM} conference on {Wireless} network security}, (Hamburg Germany), pp.~109--114, ACM, June 2011.

\bibitem{wikipedia_johnsonnyquist_2024}
Wikipedia, ``Johnson–{Nyquist} noise,'' Mar. 2024.
\newblock Page Version ID: 1214036160.

\bibitem{wikipedia_atmospheric_2024}
Wikipedia, ``Atmospheric noise,'' Jan. 2024.
\newblock Page Version ID: 1195674544.

\bibitem{dodis_fuzzy_2004}
Y.~Dodis, L.~Reyzin, and A.~Smith, ``Fuzzy {Extractors}: {How} to {Generate} {Strong} {Keys} from {Biometrics} and {Other} {Noisy} {Data},'' in {\em Advances in {Cryptology} - {EUROCRYPT} 2004} (C.~Cachin and J.~L. Camenisch, eds.), (Berlin, Heidelberg), pp.~523--540, Springer, 2004.

\bibitem{tuyls_rfid-tags_2006}
P.~Tuyls and L.~Batina, ``{RFID}-{Tags} for {Anti}-counterfeiting,'' in {\em Topics in {Cryptology} – {CT}-{RSA} 2006} (D.~Pointcheval, ed.), (Berlin, Heidelberg), pp.~115--131, Springer, 2006.

\bibitem{kong_pufatt_2014}
J.~Kong, F.~Koushanfar, P.~K. Pendyala, A.-R. Sadeghi, and C.~Wachsmann, ``{PUFatt}: {Embedded} {Platform} {Attestation} {Based} on {Novel} {Processor}-{Based} {PUFs},'' in {\em Proceedings of the 51st {Annual} {Design} {Automation} {Conference}}, {DAC} '14, (New York, NY, USA), pp.~1--6, Association for Computing Machinery, June 2014.

\bibitem{noauthor_introduction_nodate}
``Introduction to {Device} {Trust} {Architecture}.''

\bibitem{wulf_survey_2021}
C.~Wulf, M.~Willig, and D.~Göhringer, ``A {Survey} on {Hypervisor}-based {Virtualization} of {Embedded} {Reconfigurable} {Systems},'' in {\em 2021 31st {International} {Conference} on {Field}-{Programmable} {Logic} and {Applications} ({FPL})}, pp.~249--256, Aug. 2021.
\newblock ISSN: 1946-1488.

\bibitem{bobda_synthesis_2019}
C.~Bobda, T.~Whitaker, J.~M. Mbongue, and S.~K. Saha, ``Synthesis of {Hardware} {Sandboxes} for {Trojan} {Mitigation} in {Systems} on {Chip},'' in {\em 2019 {IEEE} {High} {Performance} {Extreme} {Computing} {Conference} ({HPEC})}, pp.~1--6, Sept. 2019.
\newblock ISSN: 2643-1971.

\bibitem{jin_kang_iotbox_2021}
H.~Jin~Kang, S.~Qin~Sim, and D.~Lo, ``{IoTBox}: {Sandbox} {Mining} to {Prevent} {Interaction} {Threats} in {IoT} {Systems},'' in {\em 2021 14th {IEEE} {Conference} on {Software} {Testing}, {Verification} and {Validation} ({ICST})}, pp.~182--193, Apr. 2021.
\newblock ISSN: 2159-4848.

\bibitem{wikipedia_sandbox_2024}
Wikipedia, ``Sandbox (computer security),'' Mar. 2024.
\newblock Page Version ID: 1216002953.

\end{thebibliography}
