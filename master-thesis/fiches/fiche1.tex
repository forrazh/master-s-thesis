% Fiche nº1

\chapter{Sheet nº1} % Main appendix title
\label{app:Fiche1} % For referencing this appendix elsewhere, use \ref{app:Fiche1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Description of the article}

\paragraph{Title~:} Computer Security
\paragraph{Link~:} https://wires.onlinelibrary.wiley.com/doi/abs/10.1002/wics.106
\paragraph{Authors list~:\newline} Dieter Gollmann
\paragraph{Authors affiliation~:\newline} Hamburg University of Technology, 21071 Hamburg, Germany
\paragraph{Name of the conference / journal~:} WIREs Computational Statistics
\paragraph{Classification of the conference / journal~:\newline} Q4 when the article was released (2010), as of today Q1
\paragraph{Number of article citations (which source?)~:\newline} 1731 (Google Scholar)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \pagebreak

\section{Synthesis of the article}

\paragraph{Question \newline} 

What are the evolving mechanisms and policies for enforcing computer security in IT architectures and 
software systems?

Computer security involves protecting sensitive resources in computer systems through policies that regulate 
access and mechanisms for enforcement. This article provides context to enforcement mechanisms by examining 
their original design for specific IT architectures. It also touches on network security and concludes with 
remarks on security evaluation.

Computer security encompasses four essential aspects. First, \textit{controlling system access} which involves regulating 
the entry points to computer systems, ensuring that only authorized users can gain entry. Second, \textit{managing 
resource access} focuses on controlling the permissions and privileges granted to users for accessing and 
utilizing system resources. Third, \textit{securing data in transit} i.e. protecting sensitive information while 
it is being transmitted between systems, preventing unauthorized interception or tampering. Finally, 
\textit{protecting applications from malicious inputs} which is about implementing measures to prevent and mitigate the 
risks posed by malicious code or inputs that can exploit vulnerabilities in software. These four fields 
collectively form the foundation of computer security, safeguarding systems, resources, data, and applications 
from potential threats and unauthorized access.


\paragraph{Possible trails (pointed by the authors) \newline}

As this is a taxonomy, we have a lot of available trails that are showcased. Some of which
would be : Regulating \textbf{Access Control} which can be described by the CIA triad, 
\textit{confidentiality}, \textit{integrity} and \textit{availabality}. While reading through this 
article, we can also learn about the \textbf{Commercial security} that is bnased upon the data
we hold rather than how the software is built. This could be described by \textit{Database Security} or 
\textit{Role-based Access control (RBAC)}, the author also puts emphasis on the following : 
\begin{quotation}
    Few operating systems support RBAC directly,
    but native access control features might be adapted for
    that purpose. RBAC is more often found in database
    management systems and workflow management
    systems. 
\end{quotation}

This quotation allows me to introduce about the \textbf{OS security} which has also been discussed, some of 
the examples that have been provided could be \textit{Discretionary Access Control (DAC)} or 
\textit{Mandatory Access Control (MAC)}.
The next point coming is the \textbf{Internet Security}, here the keystones will be \textit{Secure Channels}
such as SSL or TLS, \textit{FIrewalls} for access control and \textit{Intrusion Detection System} to identify
when an attack occurs. As for the \textbf{Software Security}, a few mechanisms can make it easier to create 
bugs in a program that could be correct -- \textit{without a malicious hand} -- such as a poor 
\textit{Memory Management} that could allow e.g. a buffer overflowing or usage of dangerous patterns like 
\textit{Code Injections} which result in executing code that wasn't supposed to be executed when it was first
written. Sometimes, the issue can come from concurrent acces which weren't initially planned, this is a 
\textit{Race Condition}. But some of these issues can be partly solved using \textit{Language-based Security},
an example of this could be the Garbage Collector in languages such as Java and C\#.
Speaking of languages, the following noteworthy topic is \textbf{Code-Based Access Control}. This is a user-centric 
approach, therefore more suitable for cases where users can be trusted. Here the two main topics explored are
\textit{Trusted Computing} and \textit{Digital Right Management (DRM)}. 
Now that we explored a bit of every topic, we can delve into \textbf{Web Security} which relates a bit to every 
previous ones. Firstly, there's the \textit{Code Origin Policies} which states that applets or cookies are 
restricted to connecting back to the domain they originated from. Secondly, \textit{Cross-Site Scripting (XSS)}
that can be described as a kind of attacks -- there are subkinds discussed here, e.g.stored XSS, and DOM-based 
XSS -- aiming to exploit the client's trust in a web server to execute malicious code. Thirdly comes 
\textit{Cross-Site Request Forgery (XSRF)}, this one is about using the trust given by the session, the difference 
with the XSS attack it that this one goes from the client to the server whereas the previous one goes from the 
server to the client. Lastly some countermeasures to this kind of attacks have been explored. These include changing
the execution model of web applications, implementing input sanitization techniques on servers and clients, and 
improving authentication mechanisms. The role of temporary secrets, message authentication codes, and client-side 
defenses using proxies are discussed as potential solutions. 
The last topic explored is the \textbf{Security Evaluation}. It focuses on assessing the security level of an IT 
system to provide customers with assurance. Security evaluation, certification, and accreditation are distinguished, 
with the former analyzing a product against generic security requirements, while the latter two involve specific 
customer requirements and deployment decisions. The influential Trusted Computer Systems Evaluation Criteria 
(Orange Book) aimed at evaluating operating systems for defense applications. However, evaluating operating systems 
for commercial security has faced challenges due to diverse user requirements and the evolving nature of functionality. 
Smart cards, on the other hand, have seen success in security evaluation, benefiting from market demand and their 
inherent role as security devices. Evaluating application software poses further difficulties, with customization and 
usability considerations for end users playing a crucial role.


\paragraph{Research Question \newline} 
How has computer security evolved in terms of policy enforcement mechanisms and architectural
layers, considering the historical foundations, technological advancements, and emerging 
challenges, such as distributed systems, web-based policies, digital rights management, and 
security evaluation of extensible software systems?

\paragraph{Approach \newline} 

The approach taken in this research article is to examine the evolving mechanisms and policies for
enforcing computer security in IT architectures and software systems, specifically focusing
on access control, resource protection, data security, and application security. 

\paragraph{Implementation of the approach \newline} 

Here there is not much to add up as it is a taxonomy. The trails are more interesting to investigate.

\paragraph{Results \newline} 

The results highlights a few keystones. Firstly, security policies are written in specific languages, often using 
lists in operating systems and firewalls. However, the limited processing capability of lists restricts the 
sophistication of policies. Turing complete policy languages can present undecidable questions about policies. 
To manage complexity, layers of indirection are introduced. The goal of research on policy languages is to strike 
a balance between language expressiveness and formal foundations. Additionally, the concept of noninterference provides 
theoretical foundations for studying separation properties between components. Virtualization allows for separate 
virtual machines for different applications, enhancing security. Different options exist for implementing a reference 
monitor within a software architecture, such as execution monitors and in-line reference monitors. Application writers 
need awareness and tools to write secure code, as all code accepting external input can be security relevant. Technological 
solutions should be complemented with organizational procedures and compliance with security best practices. End users now
play a crucial role in security management, including software installation, patching, and policy decisions.