\babel@toc {english}{}\relax 
\babel@toc {french}{}\relax 
\babel@toc {english}{}\relax 
\contentsline {chapter}{List of Figures}{vii}{chapter*.7}%
\contentsline {chapter}{Listings}{ix}{chapter*.8}%
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Working with Software}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}Over the Air (OTA) update}{7}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Concept}{7}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Assert trust}{8}{subsection.2.1.2}%
\contentsline {subsubsection}{Cryptography}{8}{section*.11}%
\contentsline {subsubsection}{Using the blockchain for certification}{10}{section*.13}%
\contentsline {section}{\numberline {2.2}Static analysis}{14}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Integrity Verification}{15}{subsection.2.2.1}%
\contentsline {subsubsection}{Concept}{15}{section*.16}%
\contentsline {subsubsection}{Remote Attestation}{16}{section*.18}%
\contentsline {subsubsection}{Time-consistency}{17}{section*.19}%
\contentsline {subsubsection}{Capturing the control-flow}{19}{section*.20}%
\contentsline {subsection}{\numberline {2.2.2}Abstract Interpretation}{21}{subsection.2.2.2}%
\contentsline {subsubsection}{Concept}{21}{section*.22}%
\contentsline {subsubsection}{How it is being used in Embedded systems}{26}{section*.24}%
\contentsline {subsection}{\numberline {2.2.3}Proof Carrying Code}{28}{subsection.2.2.3}%
\contentsline {subsubsection}{Concept}{29}{section*.27}%
\contentsline {subsubsection}{Embedded scenarii}{30}{section*.28}%
\contentsline {subsection}{\numberline {2.2.4}Summing up}{32}{subsection.2.2.4}%
\contentsline {chapter}{\numberline {3}Working with hardware}{35}{chapter.3}%
\contentsline {section}{\numberline {3.1}How different is it from software in this case study ?}{35}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Differenciation}{35}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Hardware / Software Partitioning}{36}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}System on Chip (SoC)}{36}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Dedicated crypto-engine}{37}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Intra-communication safety verifications}{39}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Reconfiguration}{40}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Physically Unclonable Functions (PUFs)}{41}{subsection.3.2.4}%
\contentsline {section}{\numberline {3.3}Virtualisation-based Security}{43}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Protected Module Architectures (PMA)}{43}{subsection.3.3.1}%
\contentsline {subsubsection}{Different kinds}{43}{section*.33}%
\contentsline {subsubsection}{Usage in updates}{45}{section*.34}%
\contentsline {subsection}{\numberline {3.3.2}Hardware Sandboxing}{46}{subsection.3.3.2}%
\contentsline {chapter}{\numberline {4}Conclusion}{49}{chapter.4}%
\contentsline {chapter}{R\'ef\'erences}{51}{section*.36}%
\contentsline {chapter}{\numberline {A}Sheet nº1}{59}{appendix.A}%
\contentsline {section}{\numberline {A.1}Description of the article}{59}{section.A.1}%
\contentsline {paragraph}{Title~:}{59}{section*.38}%
\contentsline {paragraph}{Link~:}{59}{section*.39}%
\contentsline {paragraph}{Authors list~:\newline }{59}{section*.40}%
\contentsline {paragraph}{Authors affiliation~:\newline }{59}{section*.41}%
\contentsline {paragraph}{Name of the conference / journal~:}{59}{section*.42}%
\contentsline {paragraph}{Classification of the conference / journal~:\newline }{59}{section*.43}%
\contentsline {paragraph}{Number of article citations (which source?)~:\newline }{59}{section*.44}%
\contentsline {section}{\numberline {A.2}Synthesis of the article}{60}{section.A.2}%
\contentsline {paragraph}{Question \newline }{60}{section*.45}%
\contentsline {paragraph}{Possible trails (pointed by the authors) \newline }{60}{section*.46}%
\contentsline {paragraph}{Research Question \newline }{61}{section*.47}%
\contentsline {paragraph}{Approach \newline }{62}{section*.48}%
\contentsline {paragraph}{Implementation of the approach \newline }{62}{section*.49}%
\contentsline {paragraph}{Results \newline }{62}{section*.50}%
\contentsline {chapter}{\numberline {B}Sheet nº2}{63}{appendix.B}%
\contentsline {section}{\numberline {B.1}Description of the article}{63}{section.B.1}%
\contentsline {paragraph}{Title~:}{63}{section*.51}%
\contentsline {paragraph}{Link~:}{63}{section*.52}%
\contentsline {paragraph}{Authors list~:\newline }{63}{section*.53}%
\contentsline {paragraph}{Authors affiliation~:\newline }{63}{section*.54}%
\contentsline {paragraph}{Name of the conference / journal~:}{63}{section*.55}%
\contentsline {paragraph}{Classification of the conference / journal~:\newline }{63}{section*.56}%
\contentsline {paragraph}{Number of article citations (which source?)~:\newline }{63}{section*.57}%
\contentsline {section}{\numberline {B.2}Synthesis of the article}{64}{section.B.2}%
\contentsline {paragraph}{Question \newline }{64}{section*.58}%
\contentsline {paragraph}{Possible trails (pointed by the authors) \newline }{64}{section*.59}%
\contentsline {paragraph}{Research Question \newline }{65}{section*.60}%
\contentsline {paragraph}{Approach \newline }{65}{section*.61}%
\contentsline {paragraph}{Implementation of the approach \newline }{65}{section*.62}%
\contentsline {paragraph}{Results \newline }{67}{section*.63}%
\contentsline {chapter}{\numberline {C}Sheet nº3}{69}{appendix.C}%
\contentsline {section}{\numberline {C.1}Description of the article}{69}{section.C.1}%
\contentsline {paragraph}{Title~:}{69}{section*.64}%
\contentsline {paragraph}{Link~:}{69}{section*.65}%
\contentsline {paragraph}{Authors list~:}{69}{section*.66}%
\contentsline {paragraph}{Authors affiliation~:\newline }{69}{section*.67}%
\contentsline {paragraph}{Name of the conference / journal~:\newline }{69}{section*.68}%
\contentsline {paragraph}{Classification of the conference / journal~:}{69}{section*.69}%
\contentsline {paragraph}{Number of article citations (which source?)~:\newline }{69}{section*.70}%
\contentsline {section}{\numberline {C.2}Synthesis of the article}{70}{section.C.2}%
\contentsline {paragraph}{Question \newline }{70}{section*.71}%
\contentsline {paragraph}{Possible trails (pointed by the authors) \newline }{70}{section*.72}%
\contentsline {paragraph}{Research Question \newline }{71}{section*.73}%
\contentsline {paragraph}{Approach \newline }{71}{section*.74}%
\contentsline {paragraph}{Implementation of the approach \newline }{71}{section*.75}%
\contentsline {paragraph}{Results \newline }{72}{section*.76}%
\contentsline {chapter}{\numberline {D}Sheet nº4}{75}{appendix.D}%
\contentsline {section}{\numberline {D.1}Description of the article}{75}{section.D.1}%
\contentsline {paragraph}{Title~:}{75}{section*.77}%
\contentsline {paragraph}{Link~:}{75}{section*.78}%
\contentsline {paragraph}{Authors list~:\newline }{75}{section*.79}%
\contentsline {paragraph}{Authors affiliation~:\newline }{75}{section*.80}%
\contentsline {paragraph}{Name of the conference / journal~:}{75}{section*.81}%
\contentsline {paragraph}{Classification of the conference / journal~:\newline }{75}{section*.82}%
\contentsline {paragraph}{Number of article citations (which source?)~:\newline }{75}{section*.83}%
\contentsline {section}{\numberline {D.2}Synthesis of the article}{76}{section.D.2}%
\contentsline {paragraph}{Question \newline }{76}{section*.84}%
\contentsline {paragraph}{Possible trails (pointed by the authors) \newline }{76}{section*.85}%
\contentsline {paragraph}{Research Question \newline }{77}{section*.86}%
\contentsline {paragraph}{Approach \newline }{77}{section*.87}%
\contentsline {paragraph}{Implementation of the approach \newline }{78}{section*.88}%
\contentsline {paragraph}{Results \newline }{79}{section*.89}%
