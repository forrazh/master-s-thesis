%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Chapter Template

\chapter{Introduction} 	% Main chapter title
\label{Chapter1} 		% For referencing the chapter elsewhere, usage \ref{Chapter1}


Nowadays, embedded systems usage has been more and more widespread. Their usage has been so 
ubiquitous that a new buzzword arose to talk about this field: \textbf{IoT} -- or 
\textbf{I}nternet \textbf{o}f \textbf{T}hings -- which emphasizes on their interconnected nature.
Whether it's a plaything you would offer to your children for Christmas, the machine handling your 
MRI scan or even inside a real airplane ; they are used everywhere for anything. By definition 
an embedded system is a microprocessor-based computer designed to perform a specific task and is not 
programmed by the end-user ; and this type of computer is usually meant to be a small piece of a 
larger system. Aloseel \textit{et al.} also add up the following to this definition : "It is 
noticeable that the main criterion in calling a system an embedded system is the embedding of 
a processing unit or the integration of computational functionality within a larger physical 
system to steer the functions of that Cyber Physcial System" \cite{aloseel_analytical_2021}. 
Those systems are often used in places that are quite critical in term of human lives, such as\textit{e.g.} 
you wouldn't want the MRI scan to jeopardize your brain or your brand new Tesla to stop moving 
while you were driving. Nevertheless, a malicious person -- an attacker -- could hack into the 
MRI scanner or your car and make it wreak havoc. From this kind of scenarii arise some security 
concerns, these concerns have remained neglected for quite a long time in the embedded system 
field but have gained more and more spotlight throughout the years. In his taxonomy, Gollman 
\cite{gollmann_computer_2010} presented the core points of Computer security, those that can 
interest us in the context of embedded systems are : Access control, OS Security, Internet 
Security and, Software Security. For this state of the art, the two that might be the most 
important are the first (Access control) and the last one (Software Security). 

Firstly, \textbf{Access Control} is about regulating access for unauthorized users. This 
topic then became the CIA triad which is characterized as \textit{\textbf{C}onfidentiality}, 
\textit{\textbf{I}ntegrity} and \textit{\textbf{A}vailability}. The first one -- \textit{
Confidentiality} -- is about preventing unauthorized disclosure of information. Here 
in our context it could be preventing a third party to access conversations in my own 
house through a hacked babyphone which I am using to monitor my baby sleeping in his 
bed. The second one -- \textit{Integrity} -- describes the fact that unauthorized 
modification shouldn't occur (as it is unauthorized). In the context of embedded systems, 
it could be something as harmless as switching two buttons on a TV's remote, the 
information I get by clicking on the button "1" is no longer the channel assigned to 1 but 
let's say the one assigned to 6. Last but not least, \textit{Availability} is about keeping
authorized users to access the protected data. For example, putting black paint on a camera's 
lens is a risk against availability \cite{gollmann_computer_2010}.

The second one, \textbf{Software security} is about having correct software without bugs 
or undefined behaviours. An undefined behaviour is a piece of code where the action cannot 
be predicted when the program was made, for example accessing memory on the go without having 
used some kind of allocator results to be an undefined behaviour because we don't know what
was their either if something else could erase it ; Most undefined behaviours results in being
bugs, an error in the flow of the program. Other kind of bugs could be -- as stated earlier -- a 
poor \textit{Memory Management} or using dangerous patterns / functions that could lead to 
\textit{Code Injection}, code that was not supposed to be executed when we first wrote it. To 
make sure a program is correct, there are multiple things that can be done such as: 1/ testing it 
empirically, ensure each small part of a code does what it needs to do ; 2/ or prooving it, 
meaning that we made mathematic specifications of the program to ensure the correctness of each 
of these specifications and that they will eventually terminate \cite{gollmann_computer_2010}.

One has to take note that the previous paragraphs stem from the general case not the embedded one.
General-purpose computers are meant to be used by anyone for anything, do multiple things at once, 
display things, run a lot of programs, can get quite powerful, and so on, and so forth. The general 
purpose computer is a Jack-of-all-trades yet, a master of none. In some cases, you need a master of
one and that's where an embedded system -- or as litterature often call them "Cyber-Physcial Systems 
(CPS)" --  can become something one needs, it is tailored to do a particular task which doesn't make 
it resource dependent and allow the manufacturer to focus the costs on enhancing other aspects such 
as fitting in its environment, e.g.: warmness, coldness, humidity, sand, etc ; or how well it can handle 
its task, e.g.: having multiple audio codecs for a headset, temperature control for a fridge, how 
grounded you desire your coffee beans before brewing it, etc. The security concerns for both of these 
fields might differ a bit too. In their review, Aloseel \textit{et al.} \cite{aloseel_analytical_2021} 
described more extensively "what is an embedded system ?" by describing those system's architecture, 
their role and more importantly their limitations. The first point comes with Figure 1.1 which describes 
how manufacturers usually craft an embedded system to make it fit its purpose perfectly. 

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{figures/aloseel_architecture.png}
    \caption{Typical architecture of embedded systems by Aloseel \textit{et al.} \cite{aloseel_analytical_2021}}
\end{figure}

In this figure, you can notice that the CPU is more or less the central point, the nerve center of the
architecture because 1/ it's less expensive to build this kind of system this way, and 2/ this allows 
easier communication between the core components of the system. Even though it is the nerve center of 
these systems, one must add components such as a memory or peripheral interfaces to communicate with the
outside world \cite{aloseel_analytical_2021}. Finally, they also introduce us to the concept of 
Systems-on-Chip (SoC) which is an integrated circuit that integrates all of the required components 
for the given system. They then describe their role as a way to make the computer interact with 
the real world through sensors and actuators.

Finally, they summarize the main limitations that exist in the embedded platform being: 1/ their 
processing capabilities, they cannot run advanced solution to defend against attacks like a 
general purpose computer would with for example an anti-malware ; 2/ their power supply, which limits 
the resources that can be allocated to security ; 3/ operating in uncontrolled or harsh environments, 
which makes them vulnerable to physical attacks ; 4/ remote and unmanned operations, which impose
difficulties in controlling physical access to the device or \textit{updating it} ; and their network 
connectivity which makes them also vulnerable to remote attacks \cite{aloseel_analytical_2021}.

To understand a bit more about the security concerns in the world of IoT and embedded systems, Leloglu \cite{leloglu_review_2016}
reviewed and summarized research to find out that there are 4 layers of functionalities for IoT 
devices and networks :
\begin{itemize}
    \item Perception layer, this one brings together every threats related to sensors. Example of 
    sensors could be : RFID, heat, light, humidity or even acceleration.
    \item Network layer, to gather every wireless networks that exist in IoT.
    \item Support layer, regroups every "processing" tasks, when an input A becomes an output B.
    This layer is quite related to the next one (Application), therefore articles often treat those
    two together. 
    \item Application layer, is for the final result of what the system / network produces. The 
    author gives smart-traffic, precise agriculture and smart home as examples.
\end{itemize}
These 4 layers are what an attacker will target on an embedded system when they want to compromise it. 
Leloglu describes the following threats with the associated layers (Figure 1.2).

\begin{figure}
    \centering
    \includegraphics[scale=0.55]{./figures/leloglu_threats_layers.jpeg}
    \caption{Security threats associated with their corresponding layer by Leloglu \cite{leloglu_review_2016}}
\end{figure}
Here the most interesting to understand are the followings :

\begin{itemize}
    \item Spoofing: Falsification of data to gain an illegitimate advantage. 
    \item DoS: Denial of Service, when a legitimate user becomes unavailable temporarily or indefinetely to access data.
    \item Signal/Radio jamming: Type of DoS attack that consists on blocking the communication channel.
    \item Sinkhole attack: Redirection of every node to a given node to disrupt the network, steal information or cause nodes to fail.
    \item Wormhole attack: Type of DoS that consists to relocate data from its original source to another point that was not the one planned originally.
    \item Eavesdropping: Sniff out confidential information by listening somewhere (a sensor or a port) we should not.
    \item MitM: Man-in-the-Middle, a form of eavesdropping consisting in monitoring or control communications between two parties.
    \item Tampering: Modification of legitimate data.
    \item Session Hijacking: Exploiting security flaws in authentication and session management.
\end{itemize}

According to the layers that have been exposed, we can focus on some more of Gollman's taxonomy \cite{gollmann_computer_2010}.
If we put the spotlight on the network layer, we can study Intrusion Detection Systems (IDS) which are pieces of software able to 
recognize attack patterns on the network and alert if one is detected. Two kind of mechanisms were described at that time: 
\textit{Knowledge}-based and \textit{anomaly}-based systems, the first one relies on detecting known attacks and the second 
one is more about detecting behaviours that deviate from the normal one. As IoT is specific due to all of the existing 
limitations and the protocols used, generic IDS's may not be suitable, this is why Roy \textit{et al.} proposed a mechanism 
able to detect efficiently some of the major IoT-centric attacks using machine learning \cite{roy_lightweight_2022}. This 
approach consists of 4 main mechanisms : (A) removal of multicollinearity, they used the Variance Inflation Factor (VIF)
to generate new features -- points to look for in the model -- and drop the original ones that had a VIF too high when
compared with the rest of the dataset ; (B) sampling, as some attacks are less frequent than others, we have less data for 
these attacks, to avoid specialization on one kind of detection they undersampled the most frequent ones and oversampled 
the less ones ; (C) dimensionality reduction, to avoid overfitting -- being able to detect only data we trained upon and 
not new data -- because of the two previous points they utilized Principal Component Analysis (PCA) to turn the matrix
resulted from training to a linear dataset ; and finally (D) an effective classification algorithm, here they proposed a 
novel algorithm called "B-Stacking" \cite{roy_lightweight_2022}.

Another point that was discussed by Gollwan was Race conditions, a problem known as "TOCTOU" -- Time of Check (to) Time 
of Use" -- in the litterature \cite{gollmann_computer_2010}. A race condition is when two processes try to access the 
same data at the same time and the result changes depending on the order of execution. Carpent \textit{et al.} 
point the \verb|Inc-Lock-Ext| and \verb|Cpy-Lock & Writeback| mechanisms as able to protect against this kind of attack
\cite{carpent_temporal_2018}, we will explore what those mechanisms entail to later.

Now that we have discussed more general concepts of security in embedded systems, let's say that we evaluate that the system 
we built is 1/ weak against one of the threats or attacks that was discussed earlier and 2/ is critical enough or (inclusive) 
costs too much money to be replaced everywhere (e.g.: a car), you would want to fix the issue with an update but, how can an 
update be carried out safely on this kind of devices ? This is what this master thesis will try to answer. 

To comprehensively address security concerns in IoT and embedded systems, it is important to explore 
strategies for implementing software and firmware updates effectively. These updates serve multiple 
purposes, including patching security vulnerabilities, improving system performance, and ensuring compliance 
with evolving standards and regulations. Moreover, they enable manufacturers to proactively respond to 
emerging threats, thereby enhancing the resilience of these systems. This master thesis will delve into 
the significance of software and firmware updates in IoT and embedded systems security. Its aim will be 
to investigate the challenges associated with updating these systems securely, considering their main 
characteristics such as limited processing capabilities, resource constraints, and operating environments. 
By understanding these challenges, we can develop tailored approaches for implementing updates that minimize 
security risks and ensure the integrity and availability of these systems. Furthermore, it will explore 
state-of-the-art methods and techniques for securely updating software and firmware in IoT and embedded 
systems. 

We will delve into the practical aspects of implementing software and firmware updates in IoT and embedded systems. More specifically, we will 
explore key methodologies and techniques such as Over-the-Air (OTA) updates, static analysis for integrity verification in Chapter II ; and hardware-based 
protection mechanisms like Protected Module Architectures (PMA) and Sandboxing throughout Chapter III. 

