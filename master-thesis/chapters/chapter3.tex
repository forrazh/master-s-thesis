\chapter{Working with hardware} 	% Main chapter title
\label{Chapter3} 

\section{How different is it from software in this case study ?}
\label{sec:Ch.3.1}

\subsection{Differenciation}
\label{sec:Ch.3.1.1}

\textbf{Software} and \textbf{Hardware} are two sides of the same coin, they work together to make one piece of 
\textit{something}. But as names differ, their purpose do too. \textbf{Software} is all the \textit{high level} 
code that will be written by a developer or engineer. The term "\textit{high level}" refers to an ordered 
sequence of instructions for changing the state of a computer \textit{hardware} part. On the other hand, \textbf{hardware} 
is the physical part of a computer, may it be a hard-drive, a display monitor, the microprocessor or anything 
that we can touch, it is hardware \cite[diffen.com]{noauthor_hardware_nodate}. 

In IoT and embedded systems, there are a lot of different CPU based systems such as microprocessors, 
microcontrollers and digital signal processors (DSP), reconfigurable devices and application specific 
integrated circuits -- shortened ASIC -- \cite{salewski_diverse_2005}. Most developpers will always work 
on the first kind of devices (microprocessors / microcontrollers), programming them using C or equivalent. 
But as Salewski \textit{et al.} do point out, it is not the only option that exist. Reconfigurable devices 
-- which is the superset that contains both complex programmable logic devices (CPLD) and field programmable 
logic arrays (FPGA) -- blur out the difference between software and hardware which makes them \textit{programmable}
in a sense by using the right tools. For example in the case of their lab courses they use VHDL (a hardware 
description language) with CPLD \cite{salewski_diverse_2005}.

The rest of this thesis will focus on the main hardware mechanisms that exist to provide security during a software
or firmware update in embedded systems such as PMAs, reconfiguration, PUF or even a more recent technique that involves
sandboxing directly on the hardware (every term will be explained in due time).

\subsection{Hardware / Software Partitioning}
\label{sec:Ch.3.1.2}

The first point we will explore is how we can "partition" hardware and software. According to Apvrille and Li,
it "intends to split the functions of a system between software components (Operating Systems, application code) 
and hardware components (processors, FPGA, hardware accelerators, buses, memories, ...)" \cite{apvrille_harmonizing_2019}.
In the litterature, another term used for it is "co-design" (\textit{i.e.} Hardware/Software co-design) 
\cite{hategekimana_secure_2018,van_bulck_towards_2016,salewski_diverse_2005}.

To explain a more thoroughly, partitioning is about determining how core components of the software and core components
of the hardware interact between each other for each functionality of the system \cite{apvrille_harmonizing_2019}.
Under the term "\textit{functionality}" comes every kind of functions (in term of code), task or communication the system
can do. This approach is still quite close to the "software approaches" we studied earlier, for example Apvrille 
and Li used abstract interpretation (\ref{sec:Ch.2.2.2}) with SysML-Sec (which has been described in \cite{apvrille_sysml-sec_2015}) and 
TTool which is a FOSS "that offers modeling, verification and code generation capabilities" \cite{apvrille_harmonizing_2019,apvrille_ttool_2008}.

Apvrille and Li's approach is guided by three pinacles : \textbf{Safety}, the avoidance of system states that can 
cause personnal or property damage \cite{apvrille_harmonizing_2019} ; \textbf{Security}, which has been studied at 
length in \ref{Chapter1} ; and \textbf{Performance}, the time needed for a system to \textit{perform} its task. 
They modeled every part of their partitioned system -- namely : abstract behaviour of tasks, algorithms, communications,
architectures and hardware components -- so that they could simulate and formally verify any component. This approach 
allowed them to evaluate and iterate over safety / security aspects easily \cite{apvrille_harmonizing_2019}.

\section{System on Chip (SoC)}
\label{sec:Ch.3.2} 

Partitioning was still a lot about software. Starting from this point, we will study mechanisms that are deeper
and deeper into the hardware. The first one will be on the brink between software and hardware, System-on-(a-)Chip 
-- shortened as SoC. According to Wikipedia, a SoC is an "integrated circuit that integrates most or all components 
of a computer or other electronic system" \cite[Wikipedia]{wikipedia_system_2024}. We talked about FPGAs and ASICs 
earlier, those are used quite extensively to create SoCs. The former is expensive to produce therefore it used more 
often to prototype than as the final piece of a project (there are some special cases, one is reviewed in \ref{sec:Ch.3.3.2})
whereas the latter will be used in production because it would have been specifically designed for it \cite{noauthor_fpga_nodate}. 
Of course, SoCs are not restricted only to FPGAs and ASICs, some CPU-based architectures include SoCs. An everyday 
life example could be the Apple-designed silicon \cite{apple_support_apple_nodate} (we will come back to them soon 
enough).

\begin{figure}
    \centering
    \includegraphics[scale=0.2]{./figures/apple_soc.png}
    \caption{Apple's SoC schematic \cite{apple_support_secure_nodate}}
\end{figure}

This section will study how dedicated crypto-engine, safety verification of hardware components 
such as the AXI bus, reconfiguration and Physically Unclonable Functions are made and performed 
onto a system.

\subsection{Dedicated crypto-engine}
\label{sec:Ch.3.2.1}

We have seen in section \ref{sec:Ch.2.1.2.1} the usage of cryptography in order to establish trust 
between two peers. This section will rather explore why some systems will bring their own cryptographic 
engine that only does cryptography, such as Apple's SoC having a dedicated AES engine in both the 
\textit{general} part of the SoC and the Secure Enclave (see Fig 3.1) \cite{mandt_demystifying_nodate,apple_support_apple_nodate,apple_support_secure_nodate}.

Having a dedicated cryptographic engine allows to run cryptographic operations (for given algorithms)
more efficiently and more safely \cite{apple_support_secure_nodate}. Behind the term efficiency, studies 
show that computing cryptographic operations on the hardware directly consumes less energy, Kietzman 
\textit{et al.} are quite extensive on it. For example, they show that the processing time for classic 
cryptographic algorithms such as AES or SHA and on \textit{state-of-the-art devices (nRF52840 / EFM32)}
the hardware version operating 5 to 10 times faster than the software one and having a ciphering speed
enhanced by a factor of 20 to 30\cite{kietzmann_performance_2021}.

\begin{figure}
    \centering
    \includegraphics[scale=0.2]{./figures/keitzman_consumption_crypto_long.png}
    \caption{Performance comparison on cryptographic algorithms between software and hardware \cite{kietzmann_performance_2021}}
\end{figure}

The fourth column displayed on Fig 3.2 shows the usage of an external cryptographic chip (ATECC608A 
connected via nRF52840) \cite{kietzmann_performance_2021}. This chip was also a contestant in this 
study because is is one protected against \textit{side-channel attacks} \cite{kietzmann_performance_2021}.
A side channel attack is an "exploit that aims to gather information from or influence the program 
execution of a system by measuring or exploiting indirect effects of the system or its hardware" 
\cite[techtarget.com]{noauthor_what_nodate}. Once again, we see the trade-off between security and 
performance thanks to those two devices. 

Even though this is a huge trade-off, we have the example of Apple that still chose to propose (starting 
from A9 SoC) a crypto-engine in the Secure Enclave that is also protected against side channel attacks 
such as power analysis \cite{apple_support_secure_nodate}. The purpose behind this idea is that this 
crypto-engine is dedicated to the tasks that require the highest level of authentication / privacy -- 
such as passwords or the FaceID features -- therefore, if the information this works with are the most 
sensitive we will find on this phone (if one gets the PIN code of my phone, they can basically access 
most information available on it), we can value the overhead of protecting this dedicated crypto-engine 
to a maximum. For operations that require encryption but are less sensitive and requires more performance 
than password encryption (such as file or disk encryption / decryption), they use the dedicated crypto-engine 
\cite{apple_support_secure_nodate}.

Vliegen \textit{et al.} designed reconfigurable hardware (we will go back to the concept of reconfiguration
in \ref{sec:Ch.3.2.3}) that uses cryptography as one of its main components. They note that a cryptographic 
protocol should take three principles in account : \textit{data confidentiality}, prevent someone to read the 
communication ; \textit{data authentication}, prevent impersonation ; and \textit{mutual entity authentication}, 
ensure that both sides are communicating with the desired peer \cite{vliegen_secure_2014}. The protocol they 
designed establishes session keys dynamically, enhancing security against attacks. Utilizing public key 
cryptography reduces the need for centralized key storage, enhancing scalability and security \cite{vliegen_secure_2014}. 
Their approach is pushed by the "cryptocore" -- or to stick with the terms we used previously, a crypto-engine 
-- they realised that implements SHA256, AES128, a Random Number Generator and an Elliptic Curve Processor 
\cite{vliegen_secure_2014} that will then be used for reconfiguration.

If we go back to our context of firmware updates, Gündoğan \textit{et al.} remind us that it is quite 
infeasible to perform asymmetric cryptography on low-end embedded devices without hardware acceleration, 
as we described it throughout this whole section \cite{gundogan_reliable_2021,kietzmann_performance_2021}.
We can conclude that cryptography is an important and even mandatory element in embedded devices security.
The question is rather, "do we want safer or faster cryptography ?".


\subsection{Intra-communication safety verifications}
\label{sec:Ch.3.2.2}

If we take a deeper look at what Apple did or even Vliegen \textit{et al.} to link their crypto-engines
to the outside world, we see that there are output channels \cite{vliegen_secure_2014,apple_support_secure_nodate}.
Those output channels have to be verified, that's what Meza \textit{et al.} propose through their safety
verification method \cite{meza_safety_nodate}.

The first thing we have to note is that, this approach focuses on data buses. To be even more 
accurate, on AXI bus following the AMBA AXI Standard. A bus serves as a communication pathway 
between controller devices and peripheral devices \cite{meza_safety_nodate}. The AXI bus stall 
problem arises within this context, particularly when multiple controllers contend for access 
to a shared peripheral. In scenarii where one controller delays providing data after issuing 
a write request, it monopolizes the bus, hindering other controllers' access to the peripheral 
\cite{meza_safety_nodate}. Despite the AXI standard's lack of specification regarding time limits 
for data provision, controllers can exploit this ambiguity to unfairly affect the availability of 
the shared peripheral to other components, potentially leading to system performance degradation 
\cite{meza_safety_nodate}.

Safety verification involves using tools to specify security properties and ensure that a system 
complies with them, either through formal methods or simulation-based approaches. Due to scalability 
concerns with formal methods, simulation-based tools are often employed for safety verification, as 
exemplified by the methodology introduced by Meza \textit{et al.}, which utilize a simulation-based 
Information Flow Tracking -- shortened as IFT, which is a "verification technique that enables the 
tracking of information as it propagates through the hardware" \cite{meza_safety_nodate,hu_hardware_2021} 
-- tool along with custom \textit{trigger modules} to detect issues like the AXI bus stall problem 
\cite{meza_safety_nodate}. To address the AXI bus stall problem, Meza \textit{et al.} propose verifying 
that each controller provisions data within a specific time frame after booking the bus with a write 
request. This entails determining the acceptable delay for each controller based on system constraints 
and then ensuring that controllers adhere to these limits \cite{meza_safety_nodate}. 

In order to solve this issue, their methodology involves utilizing a custom trigger module to track the 
state of write transactions for individual controllers. This module receives inputs indicating the maximum 
delay limit for each controller and monitors the incoming and outgoing AXI signals related to write 
transactions. It outputs the controller's state regarding write transactions, categorizing it as idle, 
within the delay limit, or exceeding the limit \cite{meza_safety_nodate}. By employing this module, system 
integrators can assess controller safety and verify compliance with specified delay requirements, thereby 
mitigating the AXI bus stall problem \cite{meza_safety_nodate}.


%%% --- Conclusion --- %%%

If we try to apply this concept to firmware updates, the trigger modules can be used to monitor the state of 
write transactions during the update process. This could allow us to monitor that the firmware update 
proceeds smoothly and efficiently without encountering delays or stalls, thereby reducing the risk of 
update failures or system instability. Additionally, the modules can enforce time constraints on write 
transactions, ensuring that firmware updates are completed within a specified timeframe, which enhances 
overall system reliability and security.

\subsection{Reconfiguration}
\label{sec:Ch.3.2.3}

Now that communication between components of a system has been explored, we can take a look at 
reconfiguration. This concept is a special one among firmware updates in the sense that, when
reconfiguring the \textit{system}, you don't change only the software but also how the hardware 
will react. Due to this need to reconfigure the hardware, we need to use a board that can be 
designed and redesign \textit{on the fly}, we will explore Vliegen \textit{et al.}'s approach
using FPGAs.

Their approach is called \textit{Dynamic Partial Reconfiguration} (DPR). The word "partial" involves 
dividing the FPGA into a single static partition containing essential communication interfaces 
-- such as the "cryptocore" discussed in \ref{sec:Ch.3.2.1} -- and one or more reconfigurable 
partitions housing the main application \cite{vliegen_secure_2014}. By segmenting the FPGA in 
this manner, maximum reconfigurable resources are allocated to the main application, enhancing 
its flexibility and adaptability. Furthermore, a soft-core processor, MicroBlaze, orchestrates 
the overall system within the static partition, managing the communication between IP cores and 
handling incoming network packets. The reconfigurable partition operates independently and 
accommodates the main application, allowing for dynamic updates while minimizing downtime
\cite{vliegen_secure_2014}.

The methodology employed by Vliegen \textit{et al.} for DPR is a regular updating process, a 
server sends the \textit{bitstream} -- instead of the firmware -- divided in ciphered chunks to 
the device. Those chunks are then deciphered by using the crypto-engine that lies in the static
partition \cite{vliegen_secure_2014}. Only upon successful validation, the partial bitstream is 
written through the Internal Configuration Access Port (ICAP) for reconfiguration, updating the 
behavior of the reconfigurable partition without disrupting the main application's functionality
\cite{vliegen_secure_2014}. This approach ensures secure and efficient FPGA reconfiguration, with 
downtime limited to the time required for transferring and updating the partial bitstream \cite{vliegen_secure_2014}.

In summary, thanks to the mutable nature of the FPGAs, this approach allows to remotely change
\textit{physical} parts of a system that could have been deployed. The main downside to this 
approach is the monetary cost of FPGAs, as described earlier, their flexibility is needed mostly 
to prototype systems that will later be provided as ASICs, that cost less to produce and consume
less power \cite{noauthor_fpga_nodate}.

\subsection{Physically Unclonable Functions (PUFs)}
\label{sec:Ch.3.2.4}

The last SoC topic we will take on a hot topic of hardware security, Physically Unclonable 
Functions -- shortened as PUF \cite{prada-delgado_trustworthy_2017}. Schulz \textit{et al.}
describe them as noisy functions that are part of a physical object \cite{schulz_short_2011}.
A "physical object" refers to the hardware as those functions lie directly onto the 
electronic parts of the system \cite{apple_support_apple_nodate,prada-delgado_trustworthy_2017}. 
The term "noisy" means that those functions are susceptible to noise -- such as 
\cite[thermal (also called Johnson-Nyquist)]{wikipedia_johnsonnyquist_2024} or 
\cite[atmospheric]{wikipedia_atmospheric_2024} noises -- their return value can change 
because of the environment if we ask the function's value multiple times \cite{schulz_short_2011}.
Schulz \textit{et al.} add up on this noise that those functions can be used 
deterministically through \textit{fuzzy extractors} on embedded devices (see 
\cite{dodis_fuzzy_2004,tuyls_rfid-tags_2006}) \cite{schulz_short_2011}.

Of course, we can use this concept in the field of firmware updates. We could for example take
a deeper look at remote attestation where Castro \textit{et al.} suggested to take a look at 
propositions from : Schulz \textit{et al.}, that chose to extend existing software attestation 
protocols by introducing a PUF-based hardware checksum to include device-specific properties 
of the prover's hardware, leveraging limited external interface throughput to prevent computation 
outsourcing and assure the verifier of the original hardware's involvement in the attestation 
process \cite{schulz_short_2011} ; Kocabas \textit{et al.} -- whose attestation method derive 
from the one described by \cite{schulz_short_2011} -- describe an iterative method that integrates 
the outputs of the prover's PUF into the software attestation process, exploiting the inter-dependency 
between the software checksum algorithm and PUF outputs to ensure both device identity and software 
integrity, while addressing challenges such as efficient prediction of PUF outputs and the need for 
PUF designs with high throughput and large challenge/response space (see Figure 3.3) \cite{kocabas_poster_2011}
; and later Kong \textit{et al.} -- their approach is based on \cite{schulz_short_2011} and \cite{kocabas_poster_2011} 
-- approach introduces the integration of the attestation checksum computation with a comprehensive 
PUF system, which includes features like error correction and obfuscation networks, providing a 
more sophisticated and comprehensive solution compared to traditional PUF-based attestation methods
\cite{kong_pufatt_2014}. But Castro \textit{et al.} also emitted a warning about the fact that it might 
require additional hardware to implement the usage of PUFs, which may have an unexpected overhead cost 
\cite{castro_evinced_2017}.

\begin{figure}
    \centering
    \includegraphics[scale=0.14]{./figures/difference_between_software_and_hardware_attestation.png}
    \caption{Difference between software and hardware based attestation \cite[Figure 1]{kocabas_poster_2011}}
\end{figure}

Another method we can look at is Prada-Delgado \textit{et al.} which is directly made for 
firmware updates over BLE. Their communication use are obfuscated through a PUF-Based 
approach. This obfuscation method involves a registration phase during which symmetric 
cryptography is utilized to establish the initial communication between IoT devices and the 
server, ensuring secure transmission of information during firmware updates. During this 
phase, the devices make useof the intrinsic randomness of the SRAM start-up values generated 
with PUF, to generate unique identifiers and nonces \cite{prada-delgado_trustworthy_2017}. 
These start-up values are classified based on their characteristics, with certain cells 
chosen to obfuscate information and others to provide nonces. Through a defined operation 
involving these values and a unique random key set generated by the manufacturer, termed 
Helper Data (HD), the devices generate and store encrypted keys for future firmware 
updates \cite{prada-delgado_trustworthy_2017}. 

Following the registration, in the reconstruction phase, when a device is activated by a client, 
it utilizes the SRAM start-up values to reconstruct the original key set. Although the values used 
for obfuscation may not exactly match those recorded during registration due to potential bit flipping, 
they are manipulated using error correction codes to obtain the original key set \cite{prada-delgado_trustworthy_2017}. 
Subsequently, a Key Derivation Function (KDF) is employed, incorporating nonces from both the server and the device, to 
derive a new key set for future updates, ensuring continual security \cite{prada-delgado_trustworthy_2017}.

Their communication protocol is trustworthy due to several factors. Firstly, the encryption 
and authentication keys are utilized only once, ensuring that firmware updates are released 
by a trusted source and mitigating the risk of compromise \cite{prada-delgado_trustworthy_2017}. 
Secondly, despite potential errors such as transmission and reception errors or response delays, 
the update process is designed to proceed correctly, thereby maintaining the integrity of the 
firmware. Lastly, the protocol includes mechanisms to detect interruptions in the update process, 
such as power loss during the update, allowing the device to recover and resume the process 
seamlessly, thereby ensuring the security and reliability of the update mechanism \cite{prada-delgado_trustworthy_2017}.


%%% --- Conclusion --- %%%

The integration of PUFs into firmware updates offers several benefits and drawbacks. On the 
positive side, PUFs provide a robust mechanism for generating unique identifiers and nonces, 
enhancing the security of communication between IoT devices and servers during firmware updates. 
Additionally, PUF-based obfuscation can be used to ensure that encryption and authentication 
keys are used only once, minimizing the risk of being compromised and ensuring the integrity 
of the update process. However, as it was pointed out by Carpent \textit{et al.}, implementing 
PUFs may introduce additional hardware requirements, potentially leading to unexpected overhead costs. 

\section{Virtualisation-based Security}
\label{sec:Ch.3.3}

\subsection{Protected Module Architectures (PMA)}
\label{sec:Ch.3.3.1}

The next abyss we reach during our dive is the one containing Protected Module Architectres -- shortened as
PMAs. PMA's are "hardware extensions that enable the secure and isolated execution of software modules by 
means of fine-grained memory access control" \cite{van_bulck_towards_2016}. They are also the concept behind
"GlobalPlatform's Device Architecture Trust" \cite{noauthor_introduction_nodate} described by Siddiqui and 
Sezer \cite{siddiqui_evolution_2020}. The latters also describe the fact that PMA become more and more used
to "harness, build and maintain robust security". In Fig 3.2, they display how embedded systems have evolved 
to gain security. The \textit{(4) Virtualisation-based Security} corresponds to the step we are describing here
(and in the next section). On the other hand, you can notice \textit{(3) Dedicated Crypto-engines} and PUFs 
that were discussed earlier come into the \textit{(5) Hardware-based Trusted Computing}, which should be the 
approach we should aim for \cite{siddiqui_evolution_2020}.

\begin{figure}
    \centering
    \includegraphics[scale=0.12]{./figures/sidiqui_sezer_5_phases.png}
    \caption{Evolution of Embedded platform security architecture \cite{siddiqui_evolution_2020}}
\end{figure}

This section will further detail the different PMA that exist nowadays and how they are used security-wise.

\subsubsection{Different kinds}
\label{sec:Ch.3.3.1.1}

\textit{Vendors} like Intel, ARM and Risc-V have developped their own solutions, namely Intel 
SGX, ARM-TrustZone and Hex-FIVE Multizone Security. Each of these three have their own specificities 
but some base foundation remain within GlobalPlatform's Device Trust Architecture framework. 

First, they all share a \textit{virtualization-based security} approach to create \textit{isolated} 
environments within the processor. These isolated domains, often referred to as "secure worlds," are designed 
to protect sensitive data and code from unauthorized access or modification \cite{siddiqui_evolution_2020}. 
Secondly, each implementation employs a form of hardware-enforced memory and peripheral isolation 
to maintain separation between secure and non-secure domains. Finally, all three technologies aim 
to minimize the risk of human error and software vulnerabilities by implementing lightweight and
responsive security solutions, such as bare-metal kernels or policy-driven security environments 
\cite{siddiqui_evolution_2020}. By prioritizing hardware-based security mechanisms and minimizing 
the reliance on complex software stacks, these implementations strive to enhance the robustness 
and resilience of the overall system architecture against potential security threats and attacks 
\cite{siddiqui_evolution_2020}.

As for distinct features, Intel SGX employs enclaves to partition sensitive information, accompanied by a 
system call mechanism for transitioning between trusted and non-trusted execution environments, albeit 
vulnerable to exploitation via synchronous and asynchronous entry/exit points \cite{siddiqui_evolution_2020}. 
ARM's TrustZone, on the other hand, introduces a dual-domain system architecture, comprising secure 
and non-secure worlds, governed by a security monitor and enforced by address space controllers 
\cite{siddiqui_evolution_2020}. Finally, Hex-FIVE MultiZone Security offers a fine-grained compartmentalization 
approach through a lightweight bare-metal kernel, facilitating policy-driven hardware-enforced 
separation of resources and utilizing physical memory protection controllers to filter data communication 
requests \cite{siddiqui_evolution_2020}.
 
On the other hand, Van Bulck \textit{et al.} present lighter solutions such as the Sancus 
architecture which extends the concept of Self-Protecting Modules (SPMs) to low-end platforms, 
such as TI MSP430 microcontrollers, facilitating hardware-level module protection \cite{van_bulck_towards_2016}. 
SPMs represent a safeguarding mechanism within shared address spaces, delineated by a public code 
segment and a private data segment. They are accessible only through predefined entry points, 
maintaining an independent call stack to insulate control flow from external influence. Memory 
access within an SPM is regulated by Protected Memory Accesses, where the program counter must
align with the corresponding code section for access \cite{van_bulck_towards_2016}.

Through remote attestation (\ref{sec:Ch.2.2.1.2}), Sancus assures software deployment integrity and confidentiality, 
employing symmetric cryptographic keys derived from module contents. Unique module identifiers 
expedite authentication and access control enforcement. Sancus automates SPM creation via a 
dedicated C compiler, embedding secure entry and exit code stubs for integrity verification and 
context management. TrustLite and TyTAN architectures offer alternative approaches, featuring 
configurable hardware memory protection units and trusted software layers for dynamic loading 
and attestation. These architectures collectively enhance embedded system security through 
hardware-enforced module protection and runtime integrity verification \cite{van_bulck_towards_2016}.

\subsubsection{Usage in updates}
\label{sec:Ch.3.3.1.2}

Knowing those virtualization-based approaches, one reading this master thesis would like to 
understand why they would be useful for firmware updates. Van Bulck \textit{et al.} point out
multiple mechanisms that would still guarantee Real-Time availability such as protecting the 
interruptions through a trusted software layer rather than totally disabling them as it was 
done in \cite[SMART]{eldefrawy_smart_2012} or having trusted threads that could handle this 
process rather than untrusted one \cite{van_bulck_towards_2016}. 

Other approaches based on Siddiqui and Sezer's could be to implement active defense mechanisms 
that integrate hardware and software layers to detect and respond to malicious activities in 
real-time. Furthermore, trust-based security enhancements should incorporate runtime response 
and recovery functions to complement passive defense measures, particularly focusing on vulnerabilities 
targeting runtime Trusted Execution Environments (TEEs) and microarchitecture \cite{siddiqui_evolution_2020}. 
Moreover, access control mechanisms must be enhanced to provide robust runtime security, utilizing 
distributed access control primitives for fine-grained software compartmentalization and detection 
of policy violations, thereby facilitating efficient response and recovery actions against malicious 
activities \cite{siddiqui_evolution_2020}.


%%% --- Conclusion --- %%%

In conclusion, PMAs can be of use for firmware updates, providing robust security measures to 
safeguard sensitive data and code from unauthorized access or modification. Virtualization-based 
security approaches create isolated environments within processors, minimizing the risk of human 
error and software vulnerabilities. However, drawbacks include potential exploitation vulnerabilities, 
as seen with Intel SGX's enclave entry/exit mechanisms. Additionally, while PMAs enhance security, 
they may introduce complexities in system architecture and require careful consideration to ensure 
seamless integration with firmware update processes.

\subsection{Hardware Sandboxing}
\label{sec:Ch.3.3.2}

Finally, the last concept this master thesis will explore is \textbf{sandboxing} and more appropriately 
"Hardware Sandboxing". For now this iteration of the concept has been seen rather on higher end FPGAs 
used in edge computing \cite{wulf_survey_2021,bobda_synthesis_2019,hategekimana_secure_2018} but more conventional
sandboxing options exist for IoT devices and networks, for example, Jin Kang \textit{et al.} presented 
IoTBox for smarthouses monitoring \cite{jin_kang_iotbox_2021}. Here we will go on with the \textbf{Virtualisation-
based security} that was studied earlier \ref{sec:Ch.3.3}.

In software security, \textbf{Sandboxing} is a mechanism used to run untrusted softwares in 
an isolated environment. This is done to avoid malicious code that can spread on the whole machine 
\cite{wikipedia_sandbox_2024}. Hardware sandboxing is a concept that derives from it and was 
introduced by Mead \textit{et al.} in \cite{mead_defeating_2016}. As it is presented by Bobda 
\textit{et al.}, hardware sandboxes are the opposite of \textit{ARM's TrustZone} we studied 
earlier. In "opposite", we have to understand that the TrustZone isolates trusted components and
marks them as secured thus making them hard to be accessed by other components that can act 
\textit{freely} anywhere else, while Hardware Sandboxing is more about letting trusted components 
\textit{live as they want} and restricting the access to untrusted components that will be 
\textit{quarantined} in their sandboxes \cite{bobda_synthesis_2019}.

This approach integrates \textit{checker components} and \textit{virtual resources}, along with 
controller and configuration registers, to monitor and enforce security rules defined by the 
system integrator \cite{mead_defeating_2016}. \textbf{Checker components} are designed to inspect 
the properties of signals associated with IP components within the sandbox, utilizing techniques 
such as the \textit{Open Verification Library} (OVL) to define and enforce signal properties. 
\textbf{Virtual resources} within the sandbox emulate shared resources between trusted systems 
and potentially compromised components, ensuring secure communication protocols to prevent 
denial-of-service attacks \cite{mead_defeating_2016}. Additionally, status and configuration 
registers facilitate communication between the sandbox manager and the system, enabling monitoring 
of IP behavior and facilitating data exchange within the sandbox. The sandbox manager oversees data 
exchange, handles results from checkers, configures the sandbox, and manages virtual and physical 
resource interactions, ensuring a secure environment for hardware operations \cite{mead_defeating_2016}.

\begin{figure}
    \centering
    \includegraphics[scale=0.25]{./figures/hw_sandbox_integration.png}
    \caption{Integration of hardware sanboxes described by Mead \textit{et al.} \cite{mead_defeating_2016}}
\end{figure}

Mead \textit{et al.} took this approach to harden drones anti-jamming systems and integrated at
the boundary of the external radio-frequency (RF) receiver. Fig 3.5 shows the \textit{wiring} of
this system. This setup ensures that only legitimate signals, compliant with the protocol, are 
forwarded to the flight control system via a virtual receiver, thereby mitigating the impact of 
jamming attacks on the overall system performance \cite{mead_defeating_2016}.

Bobda \textit{et al.} took this approach a bit farther and developed a framework named \textit{Component 
Authentication Process for Sandboxed Layouts} (CAPSL) \cite{bobda_synthesis_2019}. It aims at 
automating the generation of sandboxes from hardware IPs, represented as components with associated 
resource access rules in \textit{Property Specification Language} (PSL). This framework takes inputs of 
components and PSL rules and produces complete sandboxes suitable for integration into System 
on Chips (SoCs) \cite{bobda_synthesis_2019}. The generation process involves formal modeling of 
interfaces using \textit{Interface Automata} -- shortened IA, they are a shared boundary that acts 
as a communication channel between components -- and extracting \textit{Linear Time Logic} (LTL) 
representations of PSL rules. These representations are combined to create behavioral standards for 
sandbox monitoring \cite{bobda_synthesis_2019}. Leveraging efficient composition and refinement 
techniques, sandboxes are optimized to reduce resource requirements, such as by consolidating 
interfaces of interacting IPs. The generation process also includes \textit{Labeled Transition System}s 
(LTS) generation, where IA are mapped to corresponding LTS, and optimizations are applied using a 
forward-matching algorithm and one-hot coding technique to minimize flip-flop usage and improve 
design compactness and speed. This automated approach streamlines the design flow from specification 
to implementation, facilitating the generation of VHDL code and drivers for FPGA integration and 
enabling compatibility with Xilinx Vivado, Altera Quartus, and SystemC/TLM for high-level simulation 
\cite{bobda_synthesis_2019}.

Hardware sandboxing could be used in firmware updates to ensure the security and integrity 
of the update process. By isolating the firmware update process within a sandbox, potentially 
malicious firmwares or updates can be contained and prevented from affecting the rest of the 
system. Sandboxing also allows for monitoring and enforcing security rules during the update 
process, ensuring that only legitimate updates are applied, thereby reducing the risk of 
unauthorized access or tampering. Additionally, automated generation frameworks like CAPSL 
facilitate the creation of sandboxed environments for firmware updates, streamlining the 
development and integration of secure update mechanisms into hardware systems.


%%% --- Conclusion --- %%%

To conclude hardware sandboxing's largest drawback is that even though their overhead in 
performances is not tremendous, drones are considered as \textit{higher end} systems, they 
often bring in an OS, which is not doable on \textit{lower end} systems that may lack the 
adequate resources for such a process. On the other hand, sandboxing data received before 
passing them to the system allows to certify that data will not be harmful for the system.
