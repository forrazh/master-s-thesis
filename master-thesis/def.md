Embedded System: Microprocessor-based computer designed to do a specific task and not programmed by the end-user. This type of computer is usually meant to be a small piece of a larger system.
